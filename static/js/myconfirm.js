var myconfirm = function (opts) {
  var confirm = false;
  var dialog = BootstrapDialog.show({
    title: opts.title || _("Confirmation"),
    message: opts.question || _("Do you confirm?"),
    autodestroy: true,
    cssClass: opts.css_class || null,
    type: opts.type || BootstrapDialog.TYPE_LIGHT,
    draggable: opts.draggable || false,
    data: {
      oncancel: opts.oncancel,
      onconfirm: opts.onconfirm,
      data: opts.data,
      confirm: false,
    },
    buttons: [
      {
        label: opts.cancel_label || _("Cancel"),
        action: function (dialog) {
          dialog.close();
        },
      },
      {
        label: opts.confirm_label || _("Validate"),
        cssClass: "btn-danger",
        action: function (dialog) {
          dialog.setData("confirm", true);
          dialog.close();
        },
      },
    ],
    onhidden: function (dialog) {
      if (dialog.getData("confirm")) {
        if (jQuery.type(dialog.getData("onconfirm")) == "function") {
          dialog.getData("onconfirm")(dialog.getData("data"));
        }
      } else {
        if (jQuery.type(dialog.getData("oncancel")) == "function") {
          dialog.getData("oncancel")(dialog.getData("data"));
        }
      }
    },
  });
  return dialog;
};

var myalert = function (msg, title, opts) {
  if (!opts) opts = {};
  var dialog = BootstrapDialog.show({
    title: title || opts.title || _("Error"),
    message: msg,
    autodestroy: true,
    type: opts.type || BootstrapDialog.TYPE_DANGER,
    size: opts.size || BootstrapDialog.SIZE_MEDIUM,
    draggable: opts.draggable || false,
    cssClass: opts.css_class || null,
    data: {
      onclose: opts.onclose,
      data: opts.data,
    },
    buttons: [
      {
        label: opts.btnLabel || _("OK"),
        cssClass: opts.btnCssClass || "btn-primary",
        action: function (dialog) {
          dialog.close();
        },
      },
    ],
    onhidden: function (dialog) {
      if (jQuery.type(dialog.getData("onclose")) == "function") {
        dialog.getData("onclose")(dialog.getData("data"));
      }
    },
  });
  return dialog;
};

var myprompt = function (opts) {
  if ($.type(opts) != "object") {
    opts = {};
  }
  var submitted = false;

  var onSubmitBtnClick = function (dialog) {
    submitted = true;
    var val = dialog.getModalBody().find("input").val();
    if (jQuery.type(dialog.getData("onsubmit")) == "function") {
      if (!dialog.getData("onsubmit")(val, dialog.getData("data"))) {
        if (jQuery.type(dialog.getData("onerror")) == "function") {
          dialog.getData("onerror")(val, dialog.getData("data"));
        }
        if (!dialog.getData("closeonerror")) {
          return false;
        }
      }
    }
    dialog.close();
  };

  var dialog = BootstrapDialog.show({
    title: opts.title || _("Question"),
    message:
      "<label for='myprompt_input'>" +
      (opts.label || _("Please enter your answer below:")) +
      "</label><input type='text' class='form-control' id='myprompt_input'/>",
    autodestroy: true,
    type: opts.type || BootstrapDialog.TYPE_INFO,
    size: opts.size || BootstrapDialog.SIZE_MEDIUM,
    cssClass: opts.css_class || null,
    draggable: opts.draggable || false,
    data: {
      oncancel: opts.oncancel,
      onsubmit: opts.onsubmit,
      onerror: opts.onerror,
      closeonerror: opts.closeonerror || false,
      default_answer: opts.default_answer,
      onSubmitBtnClick: onSubmitBtnClick,
      data: opts.data,
      submitted: false,
    },
    buttons: [
      {
        label: opts.cancel_label || _("Cancel"),
        action: function (dialog) {
          dialog.close();
        },
      },
      {
        label: opts.submit_label || _("Validate"),
        cssClass: "btn-danger",
        action: onSubmitBtnClick,
      },
    ],
    onshown: function (dialog) {
      var input = dialog.getModalBody().find("input");
      input.on("keyup", function (e) {
        if (e.keyCode == 13) {
          dialog.getData("onSubmitBtnClick")(dialog);
        }
      });
      if (dialog.getData("default_answer")) {
        input.val(dialog.getData("default_answer"));
      }
    },
    onhidden: function (dialog) {
      if (!submitted && jQuery.type(dialog.getData("oncancel")) == "function") {
        dialog.getData("oncancel")(dialog.getData("data"));
      }
    },
  });
};

var myloadingalert = function (opts) {
  if (!opts) opts = {};
  var opened = false;
  var closed = false;
  var dialog = BootstrapDialog.show({
    title: opts.title || _("Please wait"),
    message:
      opts.message || _("Please wait while your request is being processed."),
    autodestroy: true,
    type: opts.type || BootstrapDialog.TYPE_INFO,
    size: opts.size || BootstrapDialog.SIZE_NORMAL,
    cssClass: opts.css_class || null,
    centered: opts.centered || true,
    closable: opts.closable || false,
    draggable: opts.draggable || false,
    onshown: function (dialog) {
      if (closed) dialog.close();
      opened = true;
    },
  });
  return {
    modal: dialog,
    close: function () {
      if (opened) dialog.close();
      closed = true;
    },
  };
};

$(document).ready(function () {
  // Manage .myconfirm-link
  $(".myconfirm-link").click(function (event) {
    event.preventDefault();
    myconfirm({
      title: $(this).data("myconfirm-title") || _("Confirmation"),
      question:
        "<p><strong>" +
        ($(this).data("myconfirm-question") || _("Are you sure?")) +
        "</strong></p>",
      onconfirm: function (data) {
        window.location = data.confirm_url;
      },
      data: {
        confirm_url: $(this).data("myconfirm-url"),
      },
    });
  });

  // Manage .myloading-link
  $(".myloading-link").click(function (event) {
    event.preventDefault();
    myloadingalert({
      title: $(this).data("myloading-title"),
      message: $(this).data("myloading-message"),
    });
    window.location = $(this).data("myloading-url");
  });

  // Manage .myconfirm-btn
  $(".myconfirm-btn").click(function (event) {
    if ($(this).data("myconfirm-btn-confirmed") == "1") {
      $(this).data("myconfirm-btn-confirmed", "");
      return true;
    }
    event.preventDefault();
    myconfirm({
      title: $(this).data("myconfirm-title") || _("Confirmation"),
      question:
        "<p><strong>" +
        ($(this).data("myconfirm-question") || _("Are you sure?")) +
        "</strong></p>",
      onconfirm: function (data) {
        data.btn.data("myconfirm-btn-confirmed", 1);
        data.btn.click();
      },
      data: {
        btn: $(this),
      },
    });
  });

  // Manage .myloading-btn
  $(".myloading-btn").click(function (event) {
    myloadingalert({
      title: $(this).data("myloading-title"),
      message: $(this).data("myloading-message"),
    });
  });
});
