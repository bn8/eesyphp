/*
 * I18n
 */
var translations;
var translations_data;

function _(string, ...extra_args) {
  var translated = translations ? translations.gettext(string) : string;
  translated = translated !== "" ? translated : string;
  if (extra_args)
    translated = translated.replace(/%[sd]/g, () => extra_args.shift());
  return translated;
}

function ngettext(singular, plural, n, ...extra_args) {
  var translated = translations
    ? translations.ngettext(singular, plural, n)
    : n > 1
    ? plural
    : singular;
  if (extra_args)
    translated = translated.replace(/%[sd]/g, () => extra_args.shift());
  return translated;
}

$(document).ready(function () {
  // Load Gettext translations
  translations = babel.Translations.load(
    translations_data ? translations_data : {}
  ).install();
});
