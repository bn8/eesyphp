<?php

use EesyPHP\App;
use EesyPHP\I18n;
use EesyPHP\SentrySpan;

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

// Root directory path
$script = null;
if (defined('__FILE__') && constant('__FILE__')) {  // @phpstan-ignore-line
  $script = __FILE__;
}
else {
  // Fallback method : detect path from core.php path
  foreach(get_included_files() as $script)
    if (basename($script) == 'core.php')
      break;
}
if (!$script) die('Fail to detect root directory path');
$root_dir_path = realpath(dirname($script).'/../');

// Include App's includes and vendor directories to PHP include paths
set_include_path($root_dir_path.'/includes' . PATH_SEPARATOR . get_include_path());

// Load composer autoload.php
require("$root_dir_path/vendor/autoload.php");

// Initialize EesyPHP application
App::init(
  "$root_dir_path/config.yml",
  array(
    'overwrite_config_files' => array(
      "$root_dir_path/config.local.yml",
    ),
    'templates' => array(
      'static_directories' => array(
        "$root_dir_path/static"
      ),
    ),
    'default' => array(
      // Set here your configuration parameters default value
    ),
  ),
  $root_dir_path
);

$sentry_span = new SentrySpan('core.init', 'Core initialization');

// Put here your own initialization stuff

require 'views/index.php';

$sentry_span->finish();

# vim: tabstop=2 shiftwidth=2 softtabstop=2 expandtab
