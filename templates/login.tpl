{extends file='Tpl:empty.tpl'}
{if !$include_navbar}
{block name="navbar"}{/block}
{/if}
{block name="pagetitle"}{/block}
{block name="main"}
<main class="form-signin w-100 m-auto text-center">
  <form action="login" method="POST">
    {if !$include_navbar}
    <img class="mb-4" src="{$logo_url}" alt="" width="100">
    {/if}
    <h1 class="h3 mb-3 fw-normal">{t domain=$CORE_TEXT_DOMAIN}Sign in{/t}</h1>

    {include file='Tpl:errors.tpl'}
    {include file='Tpl:messages.tpl'}

    <div class="form-floating">
      <input type="text" class="form-control" id="input_username" name="username"
        placeholder="{t domain=$CORE_TEXT_DOMAIN}Username{/t}" value="{if $remember_username && $username}{$username|escape:"quotes"}{/if}"/>
      <label for="input_username">{t domain=$CORE_TEXT_DOMAIN}Username{/t}</label>
    </div>
    <div class="form-floating">
      <input type="password" class="form-control" id="input_password" name="password"
        placeholder="{t domain=$CORE_TEXT_DOMAIN}Password{/t}"/>
      <label for="input_password">{t domain=$CORE_TEXT_DOMAIN}Password{/t}</label>
    </div>

    {if $remember_username}
    <div class="checkbox mb-3">
      <label>
        <input type="checkbox" name="remember-username" value="remember" {if $username}checked{/if}>
        {t domain=$CORE_TEXT_DOMAIN}Remember username{/t}
      </label>
    </div>
    {/if}
    
    <button class="w-100 btn btn-lg btn-primary" type="submit">
      {t domain=$CORE_TEXT_DOMAIN}Submit{/t}
    </button>
    {foreach $display_other_methods as $method => $name}
    <a class='btn btn-secondary w-100 mt-2' href='login?method={$method}&next={$next|escape:"url"}'>{$name}</a>
    {/foreach}
  </form>
</main>
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
