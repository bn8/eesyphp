{extends file='Tpl:empty.tpl'}
{block name="pagetitle"}{/block}
{block name="content"}
<div class="p-5 mb-4 bg-light rounded-3">
  <div class="container-fluid py-5">
    <h1 class="display-5 fw-bold">{t domain=$CORE_TEXT_DOMAIN}Access denied{/t}</h1>
    <p class="col-md-8 fs-4">
      {t escape=off domain=$CORE_TEXT_DOMAIN}You do not have access to this application.{/t}
    </p>
    {if $details}
    <p class="col-md-8 fs-6 fw-lighter fst-italic">
      {t 1=$details domain=$CORE_TEXT_DOMAIN}Details: %1{/t}
    </p>
    {/if}
  </div>
</div>
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
