{block name="errors"}
  {if isset($errors) && !empty($errors)}
    <div class="alert alert-warning" role="alert">
      {foreach $errors as $e}
        <p>{$e}</p>
      {/foreach}
    </div>
  {/if}
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
