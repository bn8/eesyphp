{extends file='Tpl:empty.tpl'}
{block name="pagetitle"}{/block}
{block name="content"}
<div class="p-5 mb-4 bg-light rounded-3">
  <div class="container-fluid py-5">
    <h1 class="display-5 fw-bold">{t domain=$CORE_TEXT_DOMAIN}Hello, world!{/t}</h1>
    <p class="col-md-8 fs-4">
      {t escape=off domain=$CORE_TEXT_DOMAIN}This is the default page of a new app using EesyPHP framework. Register your own templates directory and create a <em>homepage.tpl</em> file to overwrite it. You could also overwrite the URL handler for the root of the web application.{/t}
    </p>
  </div>
</div>
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
