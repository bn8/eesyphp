<!DOCTYPE html>
<html class="h-100" lang="fr">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <base href="{$public_root_url}/"/>
    {block name="head"}{/block}

    <link rel="icon" href="{$favicon_url}"/>

    <title>{$main_pagetitle}{if $pagetitle} - {$pagetitle}{/if}</title>

    <!-- Bootstrap -->
    <link href="{static_url path="lib/bootstrap-5.2.3/css/bootstrap.min.css"}" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="{static_url path="lib/Fork-Awesome-1.2.0/css/fork-awesome.min.css"}" rel="stylesheet"/>

    <!-- Alertify -->
    <link href="{static_url path="lib/alertify-1.14.0/css/alertify.min.css"}" rel="stylesheet"/>
    <link href="{static_url path="lib/alertify-1.14.0/css/themes/bootstrap.min.css"}" rel="stylesheet"/>

    {foreach $css as $path}
      <link href="{$path|escape:"quotes"}" rel="stylesheet"/>
    {/foreach}
  </head>

<body {block name="body-attrs"}class="d-flex flex-column h-100"{/block}>

{block name="custom_body_start"}{/block}

{block name="body"}

  {block name="navbar"}
  <header>
    <nav class="{block name="navbar-class"}navbar navbar-expand-lg{/block}" {block name="navbar-extra-args"}{/block}>
      <div class="{block name="navbar-container-class"}container{/block}">
        {block name="navbar-brand"}
        <a class="navbar-brand me-3" href="#">
          <img id="logo" src="{$logo_url}" alt="Logo" title="Logo"/>
        </a>
        {/block}

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
          data-bs-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar-menu">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            {block name="navbar-menu"}
            {/block}
          </ul>

          {block name="navbar-extra-content"}{/block}

          {block name="navbar-user"}
          <ul class="navbar-nav text-end">
            {if isset($auth_user) && $auth_user}
            <li class="nav-item dropdown">
              {block name="navbar-user-name"}
              <a class="nav-link active dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-user-circle" aria-hidden="true"></i>
                {$auth_user->name|escape:"htmlall"}
              </a>
              {/block}
              {block name="navbar-user-menu"}
              <ul class="dropdown-menu text-small">
                {block name="navbar-user-menu-content"}{/block}
                <li>
                  <a class="dropdown-item" href="logout">
                    <i class="fa fa-sign-out"></i> {t domain=$CORE_TEXT_DOMAIN}Sign out{/t}
                  </a>
                </li>
              </ul>
              {/block}
            </li>
            {else if $request->current_url != 'login'}
              {block name="navbar-login-btn"}
              <li class="nav-item">
                <a href="login" class="btn btn-sm btn-success">
                  <i class="fa fa-sign-in"></i> {t domain=$CORE_TEXT_DOMAIN}Log in{/t}
                </a>
              </li>
              {/block}
            {/if}
          </ul>
          {/block}
        </div>
      </div>
    </div>
  </header>
  {/block}

  {block name="main"}
  <main class="flex-shrink-0 mt-2 mb-2">
    <div class="container">
      {block name="pagetitle"}{if $pagetitle}<h1 class="mt-5">{$pagetitle}</h1>{/if}{/block}
      {include file='Tpl:errors.tpl'}
      {include file='Tpl:messages.tpl'}
      {block name="content"}{/block}
    </div>
  </main>
  {/block}

  {block name="footer"}
  <footer class="footer mt-auto py-3 bg-light">
    <div class="container text-center">
      {block name="footer-content"}
      {block name="footer-links"}
      <span class="text-muted">
        <a href="https://gitea.zionetrix.net/bn8/eesyphp">EesyPHP</a>
      </span>
      {/block}
      {block name="footer-compute-times"}
        {if isset($compute_time)}
        <div class='compute-times'>
          <span class="text-muted">
            {t domain=$CORE_TEXT_DOMAIN}Loading time:{/t}
            {t 1=$compute_time domain=$CORE_TEXT_DOMAIN}Page: %1{/t}
            {if isset($db_time)} - {t 1=$db_time domain=$CORE_TEXT_DOMAIN}Database: %1{/t}{/if}
            - {t domain=$CORE_TEXT_DOMAIN}Templating: {/t}{smarty_computing_time}
            - {t domain=$CORE_TEXT_DOMAIN}Total: {/t}{smarty_total_computing_time}
          </span>
        </div>
        {/if}
      {/block}
      {/block}
    </div>
  </footer>
  {/block}
{/block}


  <!-- Jquery, Bootstrap & Alertify -->
  <script src="{static_url path="lib/jquery-3.6.3.min.js"}"></script>
  <script src="{static_url path="lib/bootstrap-5.2.3/js/bootstrap.bundle.min.js"}"></script>
  <script src="{static_url path="lib/alertify-1.14.0/alertify.min.js"}"></script>

  <!-- Other libs & JavaScript scripts -->
{foreach $js as $path}
  <script language="javascript" src="{$path|escape:"quotes"}"></script>
{/foreach}

{if $webstats_js_code}{$webstats_js_code}{/if}

{block name="custom_body_end"}{/block}

  </body>
</html>
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
