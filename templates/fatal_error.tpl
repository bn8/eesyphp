{extends file='Tpl:empty.tpl'}
{block name="content"}
<div class="p-5 mb-4 bg-light rounded-3">
  <div class="container-fluid py-5">
    <h1 class="display-5 fw-bold">
      <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
      {t domain=$CORE_TEXT_DOMAIN}Oops...{/t}
    </h1>
    <p class="lead">
      {t}{t domain=$CORE_TEXT_DOMAIN}A fatal error has occurred and it is preventing this application from working properly. Please try again later or contact support.{/t}{/t}
    </p>
    <p class="fatal_error_msg">
      {t 1=$fatal_error domain=$CORE_TEXT_DOMAIN}Error: %1{/t}
    </p>
    <div class="text-center">
      {block name="buttons"}
        <a href="javascript:history.back()" class="btn btn-secondary">
          <i class="fa fa-undo"></i> {t domain=$CORE_TEXT_DOMAIN}Back{/t}
        </a>
      {/block}
    </div>
  </div>
</div>
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
