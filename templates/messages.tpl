{block name="messages"}
  {if isset($messages) && !empty($messages)}
    <div class="alert alert-info" role="alert">
      {foreach $messages as $m}
        <p>{$m}</p>
      {/foreach}
    </div>
  {/if}
{/block}
{*
# vim: autoindent expandtab tabstop=2 shiftwidth=2 softtabstop=2
*}
