# EesyPHP

EesyPHP is an easy and minimalist PHP framework for web application. It's currently
provided with a minimalist demo application that permit to manage items stored in
a database.

## Features

This project aim to provide all main necessary elements to build a web application without starting with nothing. It based on great open source libraries for that and manage their integration together and their initialization to leave you to use them without handling this boring part.

Main features are :

- a configuration manager actually based on the [yaml](https://yaml.org/) file format
- an URL routing system with a syntax based on regex
- a templating solution based on [Smarty](https://www.smarty.net/)
- an easily expandable authentication system with native HTML form, HTTP and CAS SSO support (based on [phpCAS](https://github.com/apereo/phpCAS)), database and LDAP users backends and a light user object abstraction solution with expandable attributes
- a logging system with PHP errors & exceptions handling
- a light database integration based on [FluentPDO](https://github.com/envms/fluentpdo)
- a [Sentry](https://sentry.io) integration
- a PHP session manager (with expiration and max life handling)
- a mailer solution to forge and send emails
- an hook system to easily trigger and register hooks on events
- a static files serving system with an overloadable multiple directories support
- an internationalization system based on [Gettext](https://www.gnu.org/software/gettext/) with Smarty templates (based on [Smarty Gettext](https://github.com/smarty-gettext/smarty-gettext)) and Javascript (based on [Babel JS](https://babeljs.io/)) integrations
- a command line manager to easily implement CLI tools
- collection of helpers to check, clean, cast, format (etc) values

All this features can be or not enabled and their initialization are manage by an application abstraction class.

## Installation / configuration

Please see dedicated files in _docs_ directory.

## Copyright

Copyright (c) 2020 Benjamin Renard <brenard@zionetrix.net>

## License

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
