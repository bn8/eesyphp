<?php

namespace EesyPHP\Db;

use EesyPHP\Date;

use function EesyPHP\get_system_timezone;

class AttrTimestamp extends Attr {

  /**
   * The timezone of the date
   * @var \DateTimeZone|string|null
   */
  protected $timezone = 'system';

  /**
   * The export format
   * @var string
   */
  protected $export_format = 'Y/m/d H:i:s.u';

  /**
   * Compute attribute value from DB
   * @param int|float|null $value  The value as retrieved from debug
   * @return \DateTime|null The attribute value
   */
  public function from_db($value) {
    $value = parent::from_db($value);
    if (is_null($value)) return null;
    return Date :: from_timestamp($value, $this -> timezone);
  }

  /**
   * Compute attribute value for DB
   * @param \DateTime|int|float|null $value  The value as handled in PHP
   * @return float|null The attribute value as stored in DB
   */
  public function to_db($value) {
    $value = parent::from_db($value);
    if (is_null($value)) return null;
    $value = $value instanceof \DateTime?$value:Date :: from_timestamp($value);
    return Date :: to_timestamp($value);
  }

  /**
   * Compute attribute value from string
   * @param string $value  The input value
   * @return \DateTime|null The attribute value as handled in PHP
   */
  public function from_string($value) {
    if (!$value) return null;
    $timestamp = Date :: parse($value, $this -> export_format, null, true);
    if ($timestamp === false)
      throw new DbException(
        "Error parsing date '%s' from export using format '%s'",
        $value, $this -> export_format
      );
    return $timestamp;
  }

  /**
   * Compute attribute value to string
   * @param \DateTime|int|float|null $value  The input value as handled in PHP
   * @return string The attribute value as string
   */
  public function to_string($value) {
    $value = parent::from_db($value);
    if (is_null($value)) return '';
    $value = $value instanceof \DateTime?$value:Date :: from_timestamp($value);
    return Date :: format($value->getTimestamp(), $this -> export_format);
  }

}
