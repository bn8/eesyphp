<?php

namespace EesyPHP\Db;

class AttrSet extends Attr {

  /**
   * Possible values
   * @var array<mixed>
   */
  protected $possible_values = [];

  /**
   * Compute attribute value from DB
   * @param string|null $value  The value as retrieved from debug
   * @return string|null The attribute value
   */
  public function from_db($value) {
    $value = parent::from_db($value);
    if (is_null($value))
      return null;
    if (!in_array($value, $this -> possible_values))
      throw new DbException(
        "Unexpected value '%s' retrieved from database. Should be one of the following values: %s",
        $value, implode(', ', $this -> possible_values)
      );
    return $value;
  }

  /**
   * Compute attribute value for DB
   * @param string|null $value  The value as handled in PHP
   * @return string|null The attribute value as stored in DB
   */
  public function to_db($value) {
    $value = parent::from_db($value);
    if (is_null($value))
      return null;
    if (!in_array($value, $this -> possible_values))
      throw new DbException(
        "Unexpected value '%s'. Should be one of the following values: %s",
        $value, implode(', ', $this -> possible_values)
      );
    return $value;
  }

}
