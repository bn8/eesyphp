<?php

namespace EesyPHP\Db;

class AttrInt extends Attr {

  /**
   * Auto-increment flag
   * Note: use to set this attribute as optional on creation.
   * @var bool
   */
  public $autoincrement = false;

  /**
   * Compute attribute value from DB
   * @param int|null $value  The value as retrieved from debug
   * @return int|null The attribute value
   */
  public function from_db($value) {
    $value = parent::from_db($value);
    return is_null($value)?null:intval($value);
  }

  /**
   * Compute attribute value for DB
   * @param int|null $value  The value as handled in PHP
   * @return int|null The attribute value as stored in DB
   */
  public function to_db($value) {
    $value = parent::from_db($value);
    return is_null($value)?null:intval($value);
  }

}
