<?php

namespace EesyPHP\Db;

class AttrStr extends Attr {

  /**
   * Compute attribute value from DB
   * @param string|null $value  The value as retrieved from debug
   * @return string|null The attribute value
   */
  public function from_db($value) {
    $value = parent::from_db($value);
    return is_null($value)?null:strval($value);
  }

  /**
   * Compute attribute value for DB
   * @param string|null $value  The value as handled in PHP
   * @return string|null The attribute value as stored in DB
   */
  public function to_db($value) {
    $value = parent::from_db($value);
    return is_null($value)?null:strval($value);
  }

}
