<?php

namespace EesyPHP\Db;

class AttrBool extends Attr {

  /**
   * The value stored in database for true
   * @var mixed
   */
  public static $true_value = 1;

  /**
   * The value stored in database for false
   * @var mixed
   */
  public static $false_value = 0;

  /**
   * Compute attribute value from DB
   * @param mixed $value  The value as retrieved from debug
   * @return bool|null The attribute value
   */
  public function from_db($value) {
    $value = parent::from_db($value);
    switch ($value) {
      case static :: $true_value:
        return true;
      case static :: $false_value:
        return false;
      case null:
        return null;
    }
    throw new DbException("Unknown value '%s' retrieved for %s value", $value, get_called_class());
  }

  /**
   * Compute attribute value for DB
   * @param mixed $value  The value as handled in PHP
   * @return mixed The attribute value as stored in DB
   */
  public function to_db($value) {
    $value = parent::from_db($value);
    if(is_null($value))
      return null;
    return $value?static :: $true_value:static :: $false_value;
  }

}
