<?php

namespace EesyPHP\Db;

class DbException extends \Exception {
  public function __construct($message, ...$extra_args) {
    // If extra arguments passed, format error message using sprintf
    if ($extra_args) {
      $message = call_user_func_array(
        'sprintf',
        array_merge(array($message), $extra_args)
      );
    }

    parent::__construct($message);
  }
}
