<?php

namespace EesyPHP;

use DirectoryIterator;
Use Exception;
use finfo;

use PEAR;
use Mail;
use Mail_mime;
use Smarty;


class Email {

  /**
   * Initialization
   * @return void
   */
  public static function init() {
    // Set config default values
    App :: set_default(
      'email',
      array(
        // Default sender
        'sender' => null,

        /**
         * Sending method :
         *  - mail : use PHP mail function
         *  - sendmail : use sendmail system command
         *  - smtp : use an SMTP server (PHP PEAR Net_SMTP required)
         */
        'send_method' => null,

        /**
         * Sending parameters
         * @see http://pear.php.net/manual/en/package.mail.mail.factory.php
         */
        'send_params' => array(),

        // Catch all sent email recipient
        'catch_all' => null,

        // Default headers to add on all sent emails
        'headers' => array(),

        // PHP PEAR Mail lib path
        'php_mail_path' => 'Mail.php',

        // PHP PEAR Mail_mime lib path
        'php_mail_mime_path' => 'Mail/mime.php',

        // Templates
        'templates_directory' => '${root_directory_path}/email_templates',
        'templates_cache_directory' => '${tmp_root_directory}/email_templates_c',
      )
    );
  }


  /**
   * Send an email
   *
   * @param string|null $from Email sender
   * @param string|array<string> $to Email recipient(s)
   * @param string $subject Email subject
   * @param string $msg Email body
   * @param boolean $html Set to true to send an HTML email (default: false)
   * @param array<string,string>|null $attachments Email attachments as an array with
   *                                  filepath as key and filename as value
   * @param array<string,string>|null $headers Email headers
   * @param string|null $encoding Email encoding (default: utf8)
   * @param string|null $eol End of line string (default : \n)
   *
   * @return boolean true If mail was sent, false otherwise
   */
  public static function send($from, $to, $subject, $msg, $html=false, $attachments=null,
                                   $headers=null, $encoding=null, $eol=null) {
    if (!class_exists('Mail'))
      require_once(App :: get('email.php_mail_path', null, 'string'));
    if (!class_exists('Mail_mime'))
      require_once(App :: get('email.php_mail_mime_path', null, 'string'));

    $mail_obj = Mail::factory(
      App :: get('email.send_method', null, 'string'),
      App :: get('email.send_params', null, 'array')
    );

    if (!$headers) $headers = array();
    $headers = array_merge($headers, App :: get('email.headers', null, 'array'));

    Log :: trace(
      'Mail catch all: %s',
        App :: get('email.catch_all')?
        vardump(App :: get('email.catch_all')):'not set'
    );
    if (App :: get('email.catch_all')) {
      Log :: debug(
        'Mail catch to %s',
        is_array(App :: get('email.catch_all'))?
        implode(',', App :: get('email.catch_all')):
        App :: get('email.catch_all')
      );
      $msg .= sprintf(
        (
          $html?
          I18n::_("</hr><p><small>Mail initially intended for %s.</small></p>"):
          I18n::_("\n\n\nMail initially intended for %s.")
        ),
        (is_array($to)?implode(',', $to):$to));
      $headers["X-Orig-To"] = $to;
      $to = (
        is_array(App :: get('email.catch_all'))?
        implode(',', App :: get('email.catch_all')):
        App :: get('email.catch_all')
      );
    }

    if ($subject) {
      $headers["Subject"] = $subject;
    }

    if (isset($headers['From'])) {
      if (!$from)
        $from = $headers['From'];
      unset($headers['From']);
    }
    elseif (!$from) {
      $from = App::get('email.sender');
    }

    $headers["To"] = $to;

    $to = ensure_is_array($to);

    foreach(array_keys($headers) as $header) {
      if(in_array(strtoupper($header), array('BCC', 'CC'))) {
        if (App :: get('email.catch_all')) {
          Log :: debug("Mail caught: remove $header header");
          $msg .= sprintf(
            (
              $html?
              I18n::_("<p><small>%s: %s</small></p>"):
              I18n::_("\n%s: %s")
            ),
            strtoupper($header),
            (is_array($headers[$header])?implode(',', $headers[$header]):$headers[$header]));
          unset($headers[$header]);
          continue;
        }
        $to = array_merge($to, $headers[$header]);
      }
    }

    if (!$encoding) $encoding = "utf8";
    $mime = new Mail_mime(
      array(
        'eol' => ($eol?$eol:"\n"),
        ($html?'html_charset':'text_charset') => $encoding,
        'head_charset' => $encoding,
      )
    );

    if ($from)
        $mime->setFrom($from);

    if ($subject)
      $mime->setSubject($subject);

    if ($html)
      $mime->setHTMLBody($msg);
    else
      $mime->setTXTBody($msg);

    if (is_array($attachments) && !empty($attachments)) {
      $finfo = new finfo(FILEINFO_MIME_TYPE);
      foreach ($attachments as $file => $filename) {
        $mime->addAttachment($file, $finfo->file($file), $filename);
      }
    }

    $body = $mime->get();
    $headers = $mime->headers($headers);

    $ret = $mail_obj -> send($to, $headers, $body);

    if (PEAR::isError($ret)) {
      $msg = "Error sending email: ".$ret -> getMessage();
      Log :: error($msg);
      return false;
    }
    return true;
  }

  /*
   **************************************************************************************
   *                          Templates helpers
   **************************************************************************************
   */

  /**
   * Templates directory path
   * @var string|false|null
   */
  private static $templates_directory = null;

  /**
   * Templates cache directory path
   * @var string|false|null
   */
  private static $templates_cache_directory = null;

  /**
   * Cache of templates list
   * @var array
   */
  private static $templates = [];

  /**
   * Smarty object use to compute email templates
   * @var \Smarty|null
   */
  private static $smarty = null;

  /**
   * Get templates directory path
   * @return false|string
   */
  public static function templates_directory() {
    if (self :: $templates_directory === null) {
      self :: $templates_directory  = App :: get(
        'email.templates_directory',
        self :: $templates_directory,
        "string"
      );
      if (!self :: $templates_directory || !is_dir(self :: $templates_directory))
        self :: $templates_directory = false;
    }
    return self :: $templates_directory;
  }

  /**
     * List exiting email templates
     * [
     *   '[name]' => [
     *     'subject' => '/path/to/name.subject' or null,
     *     'html' => '/path/to/name.html' or null,
     *     'txt' => '/path/to/name.txt' or null,
     *   ],
     *   [...]
     * ]
     * @return array<string,array<string,string|null>>
     */
    public static function list_templates() {
      if (self :: $templates)
        return self :: $templates;
      if (!self :: templates_directory())
        Log :: fatal("Email templates directory not initialized, can't load email templates.");
      self :: $templates = [];
      $expected_extensions = ['subject', 'html', 'txt'];
      foreach (new DirectoryIterator(self :: templates_directory()) as $fileInfo) {
        if(
          $fileInfo->isDot()
          || !$fileInfo->isFile()
          || !$fileInfo->isReadable()
          || !in_array($fileInfo->getExtension(), $expected_extensions)
        )
          continue;
        $name = $fileInfo->getBasename(".".$fileInfo->getExtension());
        if (!array_key_exists($name, self :: $templates)) {
          self :: $templates[$name] = [];
          foreach($expected_extensions as $ext) self :: $templates[$name][$ext] = null;
        }
        if (!self :: $templates[$name][$fileInfo->getExtension()])
          self :: $templates[$name][$fileInfo->getExtension()] = $fileInfo->getRealPath();
      }
      return self :: $templates;
  }

  /**
   * Return configured Smarty instance
   * @param array<string|mixed>|null $variables
   * @param bool $clear Clear previously assign variables
   * @return \Smarty
   */
  private static function smarty($variables=null, $clear=false) {
    if (self :: $smarty === null) {
      self :: $templates_cache_directory  = App :: get(
        'email.templates_cache_directory',
        sys_get_temp_dir(),
        "string"
      );
      if (!self :: $templates_cache_directory)
        Log :: fatal("Cache directory for email templates not configured.");
      else if (!is_dir(self :: $templates_cache_directory) && !mkdir(self :: $templates_cache_directory))
        Log :: fatal("Failed to create cache directory for email templates (%s)", self :: $templates_cache_directory);
      self :: $smarty = new Smarty();
      self :: $smarty -> setTemplateDir(self :: templates_directory());
      self :: $smarty -> setCompileDir(self :: $templates_cache_directory);
      self :: $smarty -> assign('public_root_url', Url :: public_root_url());
      self :: $smarty -> registerPlugin("function", "static_url", ['EesyPHP\\Tpl', 'smarty_static_url']);
      foreach(["format_size", "format_duration"] as $method)
        self :: $smarty -> registerPlugin("modifier", $method, "EesyPHP\\$method");
    }
    else if ($clear)
      self :: $smarty -> clearAllAssign();
    if ($variables) self :: $smarty->assign($variables);
    return self :: $smarty;
  }

  /**
   * Forge email from template
   * @param string $tplname The email template name
   * @param array<string,mixed> $variables Variables to use to compute the template
   * @return array<string,string|bool>|false
   */
  public static function forge_using_template($tplname, $variables=null) {
      $templates = self :: list_templates();
      if (!array_key_exists($tplname, $templates)) {
        Log :: error("forge_using_template($tplname): unknown template '$tplname'");
        return False;
      }

      $tpl = $templates[$tplname];
      if (!$tpl['subject'] || !($tpl['txt'] || $tpl['html'])) {
        Log :: error("forge_using_template($tplname): template '$tplname' is incomplete");
        return False;
      }

      $smarty = self :: smarty($variables);

      try {
        $subject = $smarty -> fetch("file:{$tpl['subject']}");
        // Multiple line from subject cause problem, trim it and only the first line
        $subject = explode("\n", trim($subject))[0];

        Log :: debug(
          "forge_using_template($tplname): subject compute from '%s'.",
          $tpl['subject']
        );
        if ($tpl['html']) {
          $message = $smarty -> fetch("file:{$tpl['html']}");
          $html = true;
          Log :: debug(
            "forge_using_template($tplname): HTML content compute from '%s'.",
            $tpl['html']
          );
        }
        else {
          $message = $smarty -> fetch("file:{$tpl['txt']}");
          $html = false;
          Log :: debug(
            "forge_using_template($tplname): text content compute from '%s'.",
            $tpl['txt']
          );
        }
      }
      catch (Exception $e) {
        Log :: exception(
          $e,
          "An exception occurred forging message from email template '%s': %s",
          $tplname, $e->getMessage()
        );
        return false;
      }

      return [
          "subject" => $subject,
          "message" => $message,
          "html" => $html,
      ];
  }

  /**
   * Send email from template
   * @param string $tplname      The email template name
   * @param string|array<string> $to The email recipient(s)
   * @param array<string,mixed> $variables    Variables to use to compute the template
   * @param string|null $from Email sender
   * @param array<string,string>|null $attachments Email attachments as an array with
   *                                  filepath as key and filename as value
   * @param array<string,string>|null $headers Email headers
   * @return boolean True if the email was sent, false otherwise
   */
  public static function send_using_template(
    $tplname, $to, $variables=null, $from=null, $attachments=null, $headers=null,
    $encoding=null, $eol=null
  ) {
    $email = self :: forge_using_template($tplname, $variables);
    if (!$email) return false;

    return self :: send(
      $from,
      ensure_is_array($to),
      $email["subject"],
      $email["message"],
      $email["html"],
      $attachments,
      $headers,
      "utf8",
      "\n"
    );
  }
}

# vim: tabstop=2 shiftwidth=2 softtabstop=2 expandtab
