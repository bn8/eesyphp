<?php

namespace EesyPHP;
use Smarty_Resource_Custom;


/**
 * Smarty resource for EesyPHP templates
 *
 * @author Benjamin Renard <brenard@zionetrix.net>
 */
class TplSmartyResource extends Smarty_Resource_Custom {

  // Core only templates
  protected $core_only;

  /**
   * Constructor
   * @param bool $core_only Core only mode (optional, default: false)
   * @return void
   */
  public function __construct($core_only=false) {
    $this -> core_only = $core_only;
  }

  /**
   * Fetch a template and its modification time
   *
   * @param string $name template name
   * @param string $source template source
   * @param integer $mtime template modification timestamp (epoch)
   * @return void
   */
  protected function fetch($name, &$source, &$mtime) {
    $source = Tpl :: get_template_source($name, $this -> core_only);
    $mtime = Tpl :: get_template_timestamp($name, $this -> core_only);
  }

  /**
   * Fetch a template's modification time
   *
   * Note: implementing this method is optional. Only implement it if modification times can be
   * accessed faster than loading the comple template source.
   *
   * @param string $name template name
   * @return integer timestamp (epoch) the template was modified
   */
  protected function fetchTimestamp($name) {
    return Tpl :: get_template_timestamp($name, $this -> core_only);
  }
}
