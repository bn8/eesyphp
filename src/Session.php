<?php

namespace EesyPHP;

/**
 * Session management
 */
class Session {

  /**
   * Session max duration (in seconds, default: 12h)
   * @var int
   */
  protected static $max_duration;

  /**
   * Initialization
   * @return void
   */
  public static function init() {
    // In CLI or Phpstan context, do not initialize
    if (
      php_sapi_name() == "cli"
      // @phpstan-ignore-next-line
      || (defined('__PHPSTAN_RUNNING__') && constant('__PHPSTAN_RUNNING__'))
    )
      return;
    // Set config default values
    App :: set_default(
      'session',
      array(
        'max_duration' => 43200,  // 12h
        'timeout' => null,
        'directory_path' => null,
      )
    );

    // Define session max duration
    self :: $max_duration = App::get('session.max_duration', null, 'int');
    Config :: ini_set('session.gc_maxlifetime', strval(self :: $max_duration));
    Config :: ini_set('session.cookie_lifetime', strval(self :: $max_duration));

    // Set custom session directory (if configured)
    $directory_path = App::get('session.directory_path', null, 'string');
    if ($directory_path) {
      Log :: trace('Set session directory path as "%s"', $directory_path);
      if (!session_save_path($directory_path)) {
        Log :: warning('Fail to set session directory path as "%s"', $directory_path);
      }
    }
    Log :: debug('Use session directory "%s"', session_save_path());

    // Start session
    session_start();

    // Init session key
    if (!isset($_SESSION['session_key'])) {
      $_SESSION['session_key'] = uniqid();
    }

    // Handle session timeout
    $timeout = App::get('session.timeout', null, 'int');
    if ($timeout) {
      if (!isset($_SESSION['session_last_access'])) {
        Log :: debug('Set initial session last access');
        $_SESSION['session_last_access'] = time();
      }
      elseif ($_SESSION['session_last_access'] > (time() - $timeout)) {
        Log :: debug(
          'Session timeout not expired, update session last access '.
          '(Previous value : %d', $_SESSION['session_last_access']);
        $_SESSION['session_last_access'] = time();
      }
      else {
        Log :: info('Session destroyed due to inactivity');
        session_destroy();
      }
    }
  }

  /**
   * Check session key
   * @param string|null $value The value of the session key to check
   *                           (optional, default: $_REQUEST['session_key'])
   * @return bool
   */
  public static function check_key($value=null) {
    if (is_null($value) && isset($_REQUEST['session_key']))
      $value = $_REQUEST['session_key'];
    return ($value && $_SESSION['session_key'] == $value);
  }

}


if (php_sapi_name() == "cli")
  return true;



# vim: tabstop=2 shiftwidth=2 softtabstop=2 expandtab
