<?php

namespace EesyPHP\Auth;

use EesyPHP\App;
use EesyPHP\Cli;
use EesyPHP\Hook;
use EesyPHP\I18n;
use EesyPHP\Log;

use Console_Table;
use Exception;

use function EesyPHP\vardump;

class Db extends Backend {

  /**
   * Database connection object
   * @var class-string|null
   */
  private static $class = null;

  /**
   * Users table name
   * @var string
   */
  private static $users_table;

  /**
   * Username field name
   * @var string
   */
  private static $username_field;

  /**
   * Password field name
   * @var string
   */
  private static $password_field;

  /**
   * List of Db fields exposed in User object
   * @var array
   */
  private static $exposed_fields;

  /**
   * Initialize
   * @return boolean
   */
  public static function init() {
    // Set config default values
    App :: set_default(
      'auth.db',
      array(
        'class' => '\\EesyPHP\\Db',
        'users_table' => 'users',
        'username_field' => 'username',
        'password_field' => 'password',
        'password_hash_algo' => 'default',
        'exposed_fields' => array('name', 'mail'),
      )
    );
    self :: $class = App::get('auth.db.class', null, 'string');
    if (!self :: $class || !class_exists(self :: $class)) {
      Log :: warning(
        'Database class %s configured as authentication backend does not exists, can not '.
        'initialize this authentication backend.', self :: $class
      );
      return false;
    }
    self :: $users_table = App::get('auth.db.users_table', null, 'string');
    self :: $username_field = App::get('auth.db.username_field', null, 'string');
    self :: $password_field = App::get('auth.db.password_field', null, 'string');
    self :: $exposed_fields = App::get('auth.db.exposed_fields', null, 'array');

    if (App :: get('cli.enabled')) {
      Cli :: add_command(
        'user',
        ['\\EesyPHP\\Auth\\Db', 'cli_user'],
        'Manage users',
        "[list|add|edit|delete]",
        [
          " list               List existing users",
          " add                Add a user",
          " edit [username]    Edit a user",
          " delete [username]  Delete a user",
        ]
      );
    }
    return true;
  }

  /**
   * Connect to database
   * @return void
   */
  private static function connect() {
    self :: $class :: connect();
  }

  /**
   * Retrieve a user by its username
   * @param string $username
   * @return \EesyPHP\Auth\User|null|false The user object if found, null it not, false in case of error
   */
  public static function get_user($username) {
    self :: connect();
    try {
      $info = self :: $class :: get_one(
        self :: $users_table,
        [self :: $username_field => $username],
        self :: $exposed_fields
      );
      if ($info === false)
        return null;
      return new User($username, '\\EesyPHP\\Auth\\Db', $info);
    }
    catch (Exception $e) {
      Log :: error("Error retrieving user %s info from database: %s", $username, $e->getMessage());
    }
    return false;
  }

  /**
   * Check a user password
   * @param \EesyPHP\Auth\User $user The user object
   * @param string $password The password to check
   * @return boolean
   */
  public static function check_password($user, $password) {
    self :: connect();
    try {
      $info = self :: $class :: get_one(
        self :: $users_table,
        [self :: $username_field => $user->username],
        [self :: $password_field]
      );
      if ($info === false)
        return false;
      return password_verify($password, $info[self :: $password_field]);
    }
    catch (Exception $e) {
      Log :: error("Error retrieving user %s password from database: %s", $user, $e->getMessage());
    }
    return false;
  }

  /**
   * Add user in database
   * @param array $info  User info with at least username, password (clear) and all required exposed
   *                     fields
   * @return bool
   */
  public static function add_user($info) {
    $values = [
      self :: $username_field => $info['username'] ?? null,
      self :: $password_field => (
        ($info['password'] ?? null)?
        password_hash(
          $info['password'],
          constant('PASSWORD_'.strtoupper(App::get('auth.db.password_hash_algo')))
        ):
        null
      ),
    ];
    foreach($info as $field => $value) {
      if (!$value) {
        Log :: error("add_user: field %s is missing (or null)", $field);
        return false;
      }
    }

    // Check username uniqueness
    if (self :: get_user($info['username'])) {
      Log :: error("add_user: a user with username %s already exist");
      return false;
    }

    foreach(App :: get('auth.db.exposed_fields') as $field)
      if (isset($info[$field]) && $info[$field])
        $values[$field] = $info[$field];

    Hook :: trigger("user_adding_in_db", $info);
    if (self :: $class :: insert(self :: $users_table, $values)) {
      Log :: info('add_user(%s): user added', $info['username']);
      Hook :: trigger("user_added_in_db", $info);
      return true;
    }
    Log :: error('add_user(%s): error adding user', $values['username']);
    return false;
  }

  /**
   * Update a user in database
   * @param \EesyPHP\Auth\User $user      The user object
   * @param array<string,mixed> $changes  Array of changes
   * @param boolean $no_change_as_success Consider no change provided as success
   *                                      (optional, default: false)
   * @return boolean True if user was updated, false otherwise
   */
  public static function update_user($user, $changes, $no_change_as_success=False) {
    Log::debug("update_user(%s): changes=%s", $user->username, vardump($changes));
    if (!$user->username) {
      Log::error("update_user(): Invalid user provided (no username)");
      return false;
    }
    if (!is_array($changes)) {
      Log::error("update_user(%s): Invalid changes provided (not an array)", $user->username);
      return false;
    }
    $values = [];
    foreach($changes as $field => $value) {
      switch ($field) {
        case "username":
          if ($value != $user->username) {
            // Check username uniqueness
            if (self :: get_user($value)) {
              Log :: error(
                "update_user(%s): invalid new username '%s': another user with this username already exist",
                $user->username,
                $value
              );
              return false;
            }
            $values[self :: $username_field] = $value;
          }
          break;
        case "password":
          if (!self::check_password($user, $value))
            $values[self :: $password_field] = password_hash(
              $value,
              constant('PASSWORD_'.strtoupper(App::get('auth.db.password_hash_algo')))
            );
          break;
        default:
          if (in_array($field, App :: get('auth.db.exposed_fields'))) {
            if ($value != $user[$field])
              $values[$field] = $value;
            break;
          }
          Log :: error("update_user: unknown field %s", $field);
          return false;
      }
    }

    if (empty($values)) {
      Log::log(
        $no_change_as_success?"DEBUG":"ERROR",
        "update_user(%s): no change",
        $user->username
      );
      return $no_change_as_success;
    }

    Log::debug("update_user(%s): changes=%s", $user->username, vardump($values));
    Hook :: trigger(
      "user_updating_in_db",
      ["user" => $user, "changes" => $changes, "raw_changes" => $values]
    );
    if (
      self :: $class :: update(
        self :: $users_table,
        $values,
        [App::get('auth.db.username_field') => $user->username]
      )
    ) {
      Log :: info('update_user(%s): user updated', $user->username);
      Hook :: trigger(
        "user_updated_in_db",
        ["user" => $user, "changes" => $changes, "raw_changes" => $values]
      );
      return true;
    }
    Log :: error('update_user(%s): error adding user', $user->username);
    return false;
  }

  /**
   * Delete a user in database
   * @param \EesyPHP\Auth\User $user      The user object
   * @return boolean True if user was deleted, false otherwise
   */
  public static function delete_user($user) {
    if (!$user->username) {
      Log::error("delete_user(): Invalid user provided (no username)");
      return false;
    }
    Hook :: trigger("user_deleting_in_db", ["user" => $user]);
    if (
      self :: $class :: delete(
        self :: $users_table,
        [self :: $username_field => $user->username]
      )
    ) {
      Log :: info('delete_user(%s): user updated', $user->username);
      Hook :: trigger("user_deleted_in_db", ["user" => $user]);
      return true;
    }
    Log :: error('delete_user(%s): error adding user', $user->username);
    return false;
  }

  /**
   * CLI command to manage users
   * @param array $command_args  Command arguments
   * @return bool
   */
  public static function cli_user($command_args) {
    if (!$command_args) Cli::usage();
    switch ($command_args[0]) {
      case "list":
        $users = self :: $class :: get_many(
          self :: $users_table,
          null,
          array_merge([self :: $username_field], self :: $exposed_fields),
          self :: $username_field
        );
        if ($users === false)
          Log :: fatal(I18n::_("Failed to list users from database."));

        if (!$users) {
          print(I18n::_("No user found."));
          return true;
        }

        $tbl = new Console_Table();
        $headers = [I18n::_("Username")];
        foreach(self :: $exposed_fields as $field)
          $headers[] = mb_convert_case(str_replace("_", " ", $field), MB_CASE_TITLE);
        $tbl->setHeaders($headers);
        foreach($users as $user) {
          $row = [$user[self::$username_field]];
          foreach(self :: $exposed_fields as $field)
            $row[] = $user[$field]?strval($user[$field]):"";
          $tbl->addRow($row);
        }
        echo $tbl->getTable();
        echo "\n".sprintf(_("%d users(s)"), count($users))."\n";
        return true;

      case "add":
        $info = ['username' => null, 'password' => null];
        foreach($info as $field => $value) {
          while(!$value) {
            $value = Cli::ask_user("Please enter user $field: ", $field == 'password');
            if (empty($value))
              print("Invalid value\n");
          }
          $info[$field] = $value;
        }
        foreach(self :: $exposed_fields as $field) {
          $value = readline("Please enter user $field: ");
          if (empty($value))
              continue;
          $info[$field] = $value;
        }
        if (self :: add_user($info)) {
          printf("User %s added\n", $info['username']);
          return true;
        }
        Log :: fatal("Error occurred adding user %s", $info['username']);

      case "edit":
        if (count($command_args) < 2)
          Cli :: usage(I18n::_("Username is missing"));
        $username = $command_args[1];
        $user = self :: get_user($username);
        if (!$user) Cli :: usage("Invalid user '$username'");
        $changes = [
          "username" => trim(
            Cli::ask_user("Please enter user new username [$username]: ")
          ),
          "password" => Cli::ask_user("Please enter user new password [empty = unchange]: ", true),
        ];
        if (!$changes["username"]) unset($changes["username"]);
        if (!$changes["password"]) unset($changes["password"]);

        foreach(self :: $exposed_fields as $field) {
          $value = Cli::ask_user("Please enter user $field [{$user[$field]}]: ");
          if (empty($value))
              continue;
          $changes[$field] = $value;
        }
        if (!$changes) {
          print("No change.\n");
          return true;
        }
        if (self :: update_user($user, $changes, true)) {
          printf("User %s updated\n", $username);
          return true;
        }
        Log :: fatal("Error occurred updating user %s", $username);

      case "delete":
        if (count($command_args) < 2)
          Cli :: usage(I18n::_("Username is missing"));
        $username = $command_args[1];
        $user = self :: get_user($username);
        if (!$user) Cli :: usage("Invalid user '$username'");
        if (self :: delete_user($user)) {
          printf("User %s deleted\n", $username);
          return true;
        }
        Log :: fatal("Error occurred deleting user %s", $username);

      default:
        Cli::usage(I18n::_("Unknown subcommand %s"), $command_args[0]);
    }
  }
}
