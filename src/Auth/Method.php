<?php

namespace EesyPHP\Auth;

use EesyPHP\Log;

class Method {

  /**
   * Initialize
   * @return boolean
   */
  public static function init() {
    return true;
  }

  /**
   * Log user
   * @param bool $force Force user authentication
   * @return \EesyPHP\Auth\User|null
   */
  public static function login($force=false) {
    Log :: fatal('login() is not implement for this authentication method.');
  }

  /**
   * Logout
   * @return void
   */
  public static function logout() {
    return;
  }
}
