<?php

namespace EesyPHP\Auth;

use EesyPHP\Hook;
use EesyPHP\Log;
use function EesyPHP\ensure_is_array;
use function EesyPHP\vardump;

class User implements \ArrayAccess {

  /**
   * Username
   * @var string
   */
  private $username;

  /**
   * User backend class name
   * @var string
   */
  private $backend;

  /**
   * User info
   * @var array<string,mixed>
   */
  private $info;

  /**
   * Original user object (set on change and keep to handle update)
   * @var User|null
   */
  private $original_user;

  /**
   * Constructor
   * @param string $username The username
   * @param string $backend The backend class name
   * @param array<string,mixed>|null $info User info (optional)
   */
  public function __construct($username, $backend, $info=null) {
    $this -> username = $username;
    $this -> backend = $backend;
    $this -> info = is_array($info)?$info:array();
  }

  /**
   * Magic method to get a dynamic property
   * @param string $key The property
   * @return mixed
   */
  public function __get($key) {
    switch ($key) {
      case 'username':
        return $this -> username;
      case 'backend':
        return $this -> backend;
      case 'info':
        return $this -> info;
      default:
        if (array_key_exists($key, $this -> info))
          return $this -> info[$key];
    }
    Log::warning(
      'Ask for unknown user property %s:\n%s', $key, Log::get_debug_backtrace_context());
    return null;
  }

  /**
   * Magic method to set a dynamic property
   * @param string $key The property
   * @param mixed $value The value
   * @return void
   */
  public function __set($key, $value) {
    Log::trace("%s->__set(%s): new value=%s", $this, $key, vardump($value));
    switch ($key) {
      case 'username':
        $this -> username = strval($value);
        break;
      case 'backend':
        $this -> backend = strval($value);
        break;
      case 'info':
        $this -> info = ensure_is_array($value);
        break;
      default:
        if (!isset($this ->original_user))
          $this -> original_user = new User($this->username, $this->backend, $this->info);
        $this -> info[$key] = $value;
    }
  }

  /**
   * Magic method to check if a dynamic property is set
   * @param string $key The property
   * @return bool
   */
  public function __isset($key) {
    switch ($key) {
      case 'username':
      case 'backend':
      case 'info':
        return true;
      default:
        return array_key_exists($key, $this -> info);
    }
  }

  /**
   * ArrayAccess interface
   */

   #[\ReturnTypeWillChange]
  /**
   * Magic method to check if an dynamic property exist when object is used as an array
   * @param string $offset The property
   * @return bool
   */
  public function offsetExists($offset) {
    return $this -> __isset($offset);
  }

  #[\ReturnTypeWillChange]
  /**
   * Magic method to get a dynamic property when object is used as an array
   * @param string $offset The property
   * @return mixed
   */
  public function offsetGet($offset){
    return $this ->  __get($offset);
  }

  #[\ReturnTypeWillChange]
  /**
   * Magic method to set a dynamic property when object is used as an array
   * @param string $offset The property
   * @param mixed $value The value
   * @return void
   */
  public function offsetSet($offset, $value) {
    $this -> __set($offset, $value);
  }

  #[\ReturnTypeWillChange]
  /**
   * Magic method to unset (=set as null) a dynamic property when object is used as an array
   * @param string $offset The property
   * @return void
   */
  public function offsetUnset($offset) {
    $this -> __set($offset, null);
  }

  /**
   * Check user password
   * @param string $password
   * @return bool
   */
  public function check_password($password) {
    return call_user_func(
      array($this -> backend, 'check_password'),
      $this, $password
    );
  }

  /**
   * Magic method to allow pretty cast to string
   * @return string
   */
  public function __toString() {
    if (isset($this -> name))
      return sprintf('%s (%s)', $this -> name, $this -> username);
    return $this -> username;
  }

  /**
   * Update user
   * @return bool
   */
  public function save() {
    if (!isset($this -> original_user)) {
      Log::debug("%s->save(): original_user not set, no change to save.", $this);
      return true;
    }
    $changes = [];
    foreach ($this->info as $attr => $value)
      if ($this->original_user->$attr != $value)
        $changes[$attr] = $value;
    if (!$changes) {
      Log::debug("%s->save(): no info updated, no change to save", $this);
      return true;
    }
    $result = call_user_func(
      array($this -> backend, 'update_user'),
      $this -> original_user, $changes, true
    );
    if ($result) {
      unset($this -> original_user);
      Hook::trigger("user_updated", ["user" => $this, "changes" => $changes]);
    }
    return $result;
  }

}
