<?php

namespace EesyPHP\Auth;

class Backend {

  /**
   * Initialize
   * @return boolean
   */
  public static function init() {
    return true;
  }

  /**
   * Retrieve a user by its username
   * @param string $username
   * @return \EesyPHP\Auth\User|null|false The user object if found, null it not, false in case of error
   */
  public static function get_user($username) {
    return null;
  }

  /**
   * Update a user
   * @param \EesyPHP\Auth\User $user      The user object
   * @param array<string,mixed> $changes  Array of changes
   * @param boolean $no_change_as_success Consider no change provided as success
   *                                      (optional, default: false)
   * @return boolean True if user was updated, false otherwise
   */
  public static function update_user($user, $changes, $no_change_as_success=False) {
    return false;
  }

  /**
   * Check a user password
   * @param \EesyPHP\Auth\User $user The user object
   * @param string $password The password to check
   * @return boolean
   */
  public static function check_password($user, $password) {
    return false;
  }
}
