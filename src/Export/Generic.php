<?php

namespace EesyPHP\Export;

class Generic {

  /**
   * Array of fields in export. Could be an associative array to specify custom exporting
   * parameters:
   * [
   *   'name',
   *   'name' => 'label',
   *   'name' => [
   *     'label' => 'Name',
   *     // all other export specific stuff
   *   ],
   * ].
   * @var array<string,array>|array<string,string>|array<string>
   */
  protected $fields;

  /**
   * Array of export parameters
   * @var array<string,mixed>
   */
  protected $parameters;

  /**
   * Array of export parameters default value
   * @var array<string,mixed>
   */
  protected static $default_parameters = [];

  /**
   * Constructor
   * @param array<string,string>|array<string> $fields Array of the fields name in export
   * @param array<string,mixed> $parameters Export parameters
   */

  public function __construct($fields, $parameters=null) {
    $this -> fields = $fields;
    $this -> parameters = is_array($parameters)?$parameters:[];
  }

  /**
   * Get parameter
   * @param string $param    Parameter name
   * @param mixed $default  Override parameter default value (optional)
   * @return mixed Parameter value
   */
  public function get_parameter($param, $default=null) {
    $default = (
      $default?
      $default:
      (
        array_key_exists($param, static :: $default_parameters)?
        static :: $default_parameters[$param]:
        null
      )
    );
    return (
      array_key_exists($param, $this -> parameters)?
      $this -> parameters[$param]:
      $default
    );
  }

  /**
   * Allow to accept parameters as properties
   * @param string $key
   * @return mixed
   */
  public function __get($key) {
    return $this -> get_parameter($key);
  }

  /**
   * Compute fields mapping info
   * @return array<string,mixed> Fields mapping info
   */
  protected function _fields_mapping() {
    $map = [];
    foreach($this -> fields as $key => $value) {
      $key = is_int($key)?$value:$key;
      $map[$key] = [
        'label' => (
          is_array($value) && array_key_exists('label', $value)?
          $value['label']:strval($value)
        ),
      ];
      // Keep all other import generic provided info and leave specific export type to handle them
      if (is_array($value))
        $map[$key] = array_merge($value, $map[$key]);
    }
    return $map;
  }

}
