<?php

namespace EesyPHP;

use JsonSerializable;

class HookEvent implements JsonSerializable {
  private $name;
  private $data;

  function __construct($name, $data) {
    $this -> name = $name;
    $this -> data = $data;
  }

  function __get($key) {
    if ($key == 'name')
      return $this -> name;
    elseif ($key == 'data')
      return $this -> data;
    elseif (is_array($this -> data) && array_key_exists($key, $this -> data))
      return $this -> data[$key];
    return null;
  }

  /**
   * Return data to serialize a HookEvent object as JSON
   * @return array<string,mixed>
   */
  #[\ReturnTypeWillChange]
  public function jsonSerialize() {
    return array (
      'name' => $this -> name,
      'data' => $this -> data,
    );
  }
}

# vim: tabstop=2 shiftwidth=2 softtabstop=2 expandtab
