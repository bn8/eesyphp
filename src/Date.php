<?php

namespace EesyPHP;


class Date {

  /**
   * The date format
   * @see strftime()
   * @var string
   */
  public static $date_format = "d/m/Y";

  /**
   * The datetime format
   * @see strftime()
   * @var string
   */
  public static $date_time_format = "d/m/Y H:i:s";

  /**
   * Format a timestamp as date/datetime string
   * @param int|\DateTime $time The timestamp to format (or a DateTime object)
   * @param bool|string $with_time_or_format If true, include time in formatted string, if string
   *                                         use it as date format (optional, default: true)
   * @return string
   */
  public static function format($time, $with_time_or_format=true) {
    $format = (
      is_string($with_time_or_format)?
      $with_time_or_format:
      ($with_time_or_format ? self :: $date_time_format : self :: $date_format)
    );
    return is_a($time, "DateTime")?$time->format($format):date($format, $time);
  }

  /**
   * Parse a date/datetime string as timestamp
   * @param string $date The date string to parse
   * @param bool|string $with_time_or_format If true, include time in formatted string, if string
   *                                         use it as date format (optional, default: true)
   * @param \DateTimeZone|string|null $timezone Timezone of the loaded date
   *                                            (optional, default: system)
   * @param bool $as_datetime If true, return a DateTime object instead of timestamp as integer
   * @return ( $as_datetime is true ? \DateTime : int )|false
   */
  public static function parse(
    $date, $with_time_or_format=true, $timezone=null, $as_datetime=false
  ) {
    $format = (
      is_string($with_time_or_format)?
      $with_time_or_format:
      ($with_time_or_format ? self :: $date_time_format : self :: $date_format)
    );
    $date = \DateTime::createFromFormat($format, $date, self :: timezone($timezone));
    if ($as_datetime || $date === false)
      return $date;
    return $date->getTimestamp();
  }

  /**
   * Get timezone
   * @param \DateTimeZone|string|null $value  Timezone name or 'system'|null for system timezone
   *                                          (optional)
   * @param bool $as_string If true, return the name of the timezone instead of a DateTimeZone
   *                        object (optional, default: false)
   * @return ($as_string is true ? string|false : \DateTimeZone|false)
   */
  public static function timezone($value=null, $as_string=false) {
    $timezone = (
      is_null($value) || $value == 'system'?
      get_system_timezone():
      timezone_open($value)
    );
    if (!$as_string || !$timezone)
      return $timezone;
    return $timezone -> getName();
  }

  /**
   * Create \DateTime object from timestamp
   * @param int $value     The timestamp
   * @param \DateTimeZone|string|null $timezone  Timezone name or 'system'|null for system timezone
   *                                             (optional)
   * @return \DateTime
   */
  public static function from_timestamp($value, $timezone=null) {
    $date = \DateTime::createFromFormat("U.u", number_format($value, 6, ".", ""));
    $date -> setTimezone(self :: timezone($timezone));
    return $date;
  }

  /**
   * Convert \DateTime object to timestamp as float with microsecond
   * @param \DateTime $value     The \DateTime object to convert
   * @return float
   */
  public static function to_timestamp($value) {
    return floatval($value->format("U.u"));
  }

  /**
   * Create \DateTime object
   * @param int|null $timezone  The expected timezone (optional, default: system one)
   * @param \DateTimeZone|string|null $timezone  Timezone name or 'system'|null for system timezone
   *                                             (optional)
   * @return \DateTime
   */
  public static function now($timezone=null) {
    $date = new \DateTime();
    $date -> setTimezone(self :: timezone($timezone));
    return $date;
  }
}
