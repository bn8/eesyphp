<?php

namespace EesyPHP;

class Check {
  public static function name($name) {
    if (preg_match('/^[\w \-]{2,}$/iu',$name))
      return true;
    return false;
  }

  public static function id(&$id) {
    if (is_int($id))
      return true;
    if (preg_match('/^[0-9]+$/', $id)) {
      $id = intval($id);
      return true;
    }
    return false;
  }

  public static function search_pattern($pattern) {
    foreach(preg_split('/\s+/', trim($pattern)) as $word) {
      if (!self :: id($word) && !self :: name($word))
        return false;
    }
    return true;
  }

  public static function time(&$time) {
    if (!is_int($time)) {
      if (preg_match('/^[0-9]+$/', $time))
        $time = intval($time);
      else
        return false;
    }
    return ($time >= 1577833200);  // 2020-01-01 - date of birth of this soft
  }

  public static function description($comment) {
    if (preg_match("/^[\p{L}0-9\p{P}\p{Zs}\p{Zl}\p{Sc}\=\+]+$/uim", $comment))
      return true;
    return false;
  }

  public static function email($value, $domains=NULL, $checkDns=true) {
    $regex = '/^((\"[^\"\f\n\r\t\v\b]+\")|([\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+(\.[\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+)*))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+))$/';

    if (!preg_match($regex, $value)) {
      return false;
    }

    $domain = explode('@', $value);
    $domain = $domain[1];

    if ($domains && !in_array($domain, ensure_is_array($domains)))
      return false;

    if ($checkDns) {
      if (!function_exists('checkdnsrr')) {
        Log::warning(
          "Check::email(): PHP function checkdnsrr is missing, can't check domain against DNS. ".
          "Assume is OK."
        );
      }
      else if (!(checkdnsrr($domain, 'MX') || checkdnsrr($domain, 'A'))) {
        return false;
      }
    }

    return true;
  }

  public static function ip_address($ip, $allow_private_ip_address=true, $allow_reserved_ip_address=true,
                      $ipv4_only=false, $ipv6_only=false) {
  	$flag = null;
  	if ($ipv4_only)
  		$flag |= FILTER_FLAG_IPV4;

  	if ($ipv6_only)
  		$flag |= FILTER_FLAG_IPV6;

  	if (!isset($allow_private_ip_address) || !$allow_private_ip_address)
  		$flag |= FILTER_FLAG_NO_PRIV_RANGE;

  	if (!isset($allow_reserved_ip_address) || !$allow_reserved_ip_address)
  		$flag |= FILTER_FLAG_NO_RES_RANGE;

  	if (!is_null($flag))
  		return (bool)filter_var($ip, FILTER_VALIDATE_IP, $flag);
  	else
  		return (bool)filter_var($ip, FILTER_VALIDATE_IP);
  }

  public static function tcp_or_udp_port($value) {
    if (!is_int($value)) {
      if (!preg_match('/^[0-9]+$/', $value))
        return false;
      $value = intval($value);
    }
    if ($value <= 0) return false;
    if ($value > 65635) return false;
    return true;
  }

  public static function is_empty($val) {
    switch(gettype($val)) {
      case "boolean":
      case "integer":
      case "double":
      case "object":
      case "resource":
        return False;
      case "array":
      case "string":
        if ($val == "0") return false;
        return empty($val);
      case "NULL":
        return True;
    }
  }

  /**
   * check if an uuid is correct
   *
   * @access public
   * @param string $value The value to check
   * @return bool true or false
   */
  public static function uuid($value) {
    if (preg_match('/^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}$/',$value)) {
      return true;
    }
    return false;
  }

  /**
   * Check value is a valid regex (PREG)
   * @param mixed  $value          The value to check
   * @param boolean $return_error  Set to true to get an error message about the error
   * @return ( $return_error is true ? string|true : boolean )
   */
  public static function regex($value, $return_error=false) {
    if (!is_string($value))
      return $return_error?'PREG ERROR: not a string':false;
    if (@preg_match($value, null) === false) {  // @phpstan-ignore-line
      if ($return_error)
        return sprintf(
          'PREG ERROR #%d%s',
          preg_last_error(),
          (
            function_exists('preg_last_error_msg')?
            sprintf(" (%s)", preg_last_error_msg()):
            ""
          )
        );
      return false;
    }
    return true;
  }

}

# vim: tabstop=2 shiftwidth=2 softtabstop=2 expandtab
