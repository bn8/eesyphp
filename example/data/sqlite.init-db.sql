CREATE TABLE users (
    username text NOT NULL PRIMARY KEY,
    name text COLLATE NOCASE NOT NULL,
    mail text COLLATE NOCASE,
    password text NOT NULL
);

INSERT INTO users (username, name, mail, password) VALUES (
  "admin", "Administrator", "admin@example.com",
  "$argon2id$v=19$m=65536,t=4,p=1$WTQ0di44NW11MUJ1b3RMQw$+LRAQRaIXE2jhfavNFNuxnEtEUT6tEBz/98pTtD0EnM"
);

CREATE TABLE item (
    id INTEGER PRIMARY KEY,
    name text COLLATE NOCASE NOT NULL,
    date INTEGER,
    status text NOT NULL,
    description text COLLATE NOCASE NULL
);
