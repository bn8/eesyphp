CREATE TABLE public.item (
    id SERIAL NOT NULL,
    name text NOT NULL,
    date timestamp with time zone,
    status text NOT NULL,
    description text NULL
);
ALTER TABLE public.item OWNER TO items;
