msgid ""
msgstr ""
"POT-Creation-Date: 2024-01-23 18:09+0000\n"
"PO-Revision-Date: 2024-01-23 18:09+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: includes/url-helpers.php:9
msgid "Invalid element identifier."
msgstr ""

#: includes/url-helpers.php:13 includes/url-public.php:232
#: includes/url-public.php:259 includes/cli.php:136 includes/cli.php:160
#, php-format
msgid "Item #%s not found."
msgstr ""

#: includes/url-public.php:38
msgid "Any"
msgstr ""

#: includes/url-public.php:96
msgid ""
"An error occurred while listing the items. If the problem persists, please "
"contact support."
msgstr ""

#: includes/url-public.php:114
msgid "Search"
msgstr ""

#: includes/url-public.php:140
#, php-format
msgid "Element %s"
msgstr ""

#: includes/url-public.php:159
#, php-format
msgid "The element '%s' has been created."
msgstr ""

#: includes/url-public.php:162
msgid "An error occurred while saving this item."
msgstr ""

#: includes/url-public.php:168
msgid ""
"There are errors preventing this item from being saved. Please correct them "
"before attempting to add this item."
msgstr ""

#: includes/url-public.php:175
msgid "New"
msgstr ""

#: includes/url-public.php:186
msgid "You cannot edit this item."
msgstr ""

#: includes/url-public.php:199
#, php-format
msgid "You have not made any changes to element '%s'."
msgstr ""

#: includes/url-public.php:203
#, php-format
msgid "The element '%s' has been updated successfully."
msgstr ""

#: includes/url-public.php:206
msgid "An error occurred while updating this item."
msgstr ""

#: includes/url-public.php:213
msgid ""
"There are errors preventing this item from being saved. Please correct them "
"before attempting to save your changes."
msgstr ""

#: includes/url-public.php:220
#, php-format
msgid "Element %s: Modification"
msgstr ""

#: includes/url-public.php:236
msgid "This item is already archived."
msgstr ""

#: includes/url-public.php:239
msgid "You cannot archive this item."
msgstr ""

#: includes/url-public.php:242
#, php-format
msgid "The element '%s' has been archived successfully."
msgstr ""

#: includes/url-public.php:245
msgid "An error occurred while archiving this item."
msgstr ""

#: includes/url-public.php:262
msgid "You cannot delete this item."
msgstr ""

#: includes/url-public.php:265
#, php-format
msgid "The element '%s' has been deleted successfully."
msgstr ""

#: includes/url-public.php:268
msgid "An error occurred while deleting this item."
msgstr ""

#: includes/core.php:61
msgid "Pending"
msgstr ""

#: includes/core.php:62
msgid "Validated"
msgstr ""

#: includes/core.php:63
msgid "Refused"
msgstr ""

#: includes/core.php:64
msgid "Archived"
msgstr ""

#: includes/cli.php:24
#, php-format
msgid "Item #%s:\n"
msgstr ""

#: includes/cli.php:25
#, php-format
msgid "ID: %s"
msgstr ""

#: includes/cli.php:26
#, php-format
msgid "Name: '%s'"
msgstr ""

#: includes/cli.php:27
#, php-format
msgid "Date: %s"
msgstr ""

#: includes/cli.php:29
#, php-format
msgid "Description: %s"
msgstr ""

#: includes/cli.php:30
msgid "Not set"
msgstr ""

#: includes/cli.php:32
#, php-format
msgid "Status: %s"
msgstr ""

#: includes/cli.php:85
msgid "No item.\n"
msgstr ""

#: includes/cli.php:111
#, php-format
msgid "%d item(s)"
msgstr ""

#: includes/cli.php:117
msgid "List/search items"
msgstr ""

#: includes/cli.php:118
msgid "[patterns]"
msgstr ""

#: includes/cli.php:120
msgid "-o|--orderby       Ordering list criterion. Possible values:"
msgstr ""

#: includes/cli.php:122
msgid "-r|--reverse       Reverse order"
msgstr ""

#: includes/cli.php:123
msgid "-s|--status        Filter on status. Possible values:"
msgstr ""

#: includes/cli.php:130
msgid "You must provide a valid ID."
msgstr ""

#: includes/cli.php:144
msgid "Show item"
msgstr ""

#: includes/cli.php:145
msgid "[ID]"
msgstr ""

#: includes/cli.php:150
msgid "You must provide item ID."
msgstr ""

#: includes/cli.php:154
msgid "Invalid item ID"
msgstr ""

#: includes/cli.php:165
msgid "Are you sure you want to delete this item?  Type 'yes' to continue: "
msgstr ""

#: includes/cli.php:169
msgid "User cancel"
msgstr ""

#: includes/cli.php:175
#, php-format
msgid "An error occurred deleting item #%d."
msgstr ""

#: includes/cli.php:182
msgid "Delete item"
msgstr ""

#: includes/cli.php:183
msgid "[item ID]"
msgstr ""

#: includes/cli.php:195
msgid "Export items (as CSV)"
msgstr ""

#: includes/cli.php:196
msgid "[output file path]"
msgstr ""

#: includes/cli.php:211
msgid "Restore items (from CSV)"
msgstr ""

#: includes/cli.php:212
msgid "[input file path]"
msgstr ""

#: includes/cli.php:279
msgid "Cron to handle item expiration"
msgstr ""

#: includes/cli.php:282
msgid "-j/--just-try    Just-try mode : do not really removed expired item(s)"
msgstr ""

#: includes/cli.php:283
msgid "-m/--max-age     Item expiration limit (in days, optional)"
msgstr ""

#: templates/form.tpl:7 templates/search.tpl:50 templates/show.tpl:5
msgid "Name"
msgstr ""

#: templates/form.tpl:17 templates/search.tpl:15 templates/search.tpl:51
#: templates/show.tpl:23
msgid "Status"
msgstr ""

#: templates/form.tpl:28 templates/show.tpl:32
msgid "Description"
msgstr ""

#: templates/form.tpl:36 templates/show.tpl:42
msgid "Back"
msgstr ""

#: templates/form.tpl:39
msgid "Save"
msgstr ""

#: templates/form.tpl:41 templates/search.tpl:41
msgid "Add"
msgstr ""

#: templates/search.tpl:8
msgid "Pattern"
msgstr ""

#: templates/search.tpl:24
msgid "Nb by page"
msgstr ""

#: templates/search.tpl:35
msgid "Reset"
msgstr ""

#: templates/search.tpl:49
msgid "Date"
msgstr ""

#: templates/search.tpl:52
msgid "Actions"
msgstr ""

#: templates/search.tpl:62
msgid "View"
msgstr ""

#: templates/search.tpl:63 templates/show.tpl:43
msgid "Modify"
msgstr ""

#: templates/search.tpl:64 templates/show.tpl:44
msgid "Are you sure you want to archive this item?"
msgstr ""

#: templates/search.tpl:64 templates/show.tpl:44
msgid "Archive"
msgstr ""

#: templates/search.tpl:65 templates/show.tpl:45
msgid "Are you sure you want to delete this item?"
msgstr ""

#: templates/search.tpl:65 templates/show.tpl:45
msgid "Delete"
msgstr ""

#: templates/search.tpl:71
msgid "No item found."
msgstr ""

#: templates/search.tpl:80
msgid "%3 element"
msgid_plural "Elements %1 to %2 on %3"
msgstr[0] ""
msgstr[1] ""

#: templates/show.tpl:14
msgid "Creation date"
msgstr ""

#: templates/show.tpl:35
msgid "Unspecified."
msgstr ""

#: templates/homepage.tpl:6
msgid "Hello, world!"
msgstr ""

#: templates/homepage.tpl:8
msgid ""
"This is a simple app to show the different possibilities and basic "
"functionality."
msgstr ""

#: templates/homepage.tpl:11
msgid ""
"The access to this application is available by the following URL: <a "
"href=\"%1\">%1</a>"
msgstr ""

#: templates/homepage.tpl:14
msgid "This app contains some demo pages:"
msgstr ""

#: templates/homepage.tpl:15
msgid "Search page"
msgstr ""
