��    U      �  q   l      0  "   1  
   T  F   _  :   �  <   �        5   ?     u     }  $   �  ,   �  +   �  [   �  )   [	  +   �	     �	     �	     �	  +   �	  *   �	  D   
     b
     g
     u
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
                     <     L  
   `     k     }     �  
   �  
   �     �     �  	   �     �     �     �     �     �     �     �             	          
   $  U   /  0   �  "   �  /   �  /   	  o   9  s   �  "     Q   @     �     �     �  	   �     �     �     �       .   /     ^     {     �     �  	   �     �  
   �  !  �  '   �       T   .  K   �  C   �        =   4     r     z  6   �  3   �  4   �  v   "  6   �  9   �     
            4   (  5   ]  S   �     �     �  (         )  	   .  	   8     B     Y     e     v     �  '   �     �     �      �               :      K     l     u     y     �     �     �     �     �     �  
   �     �     �  0   �     )  
   5     @     R     d     k  S   w  '   �  %   �  (     +   B  x   n  �   �  4   h  l   �  "   
     -     =     U     ]  *   b  +   �  *   �  =   �     "     B     b     g     �     �     �     +   '       -   F       .          J   I   U   T       P       <   2           H   4   6      K   *      0   B       	      ?   
      O   (   )   L   D   8   $   Q       =                %   >          "       3   M   1       /   ;      9                   N   #         S       ,                     A   :       7                 5   R                E                     &   G            C                   !   @    %3 element Elements %1 to %2 on %3 %d item(s) -j/--just-try    Just-try mode : do not really removed expired item(s) -m/--max-age     Item expiration limit (in days, optional) -o|--orderby       Ordering list criterion. Possible values: -r|--reverse       Reverse order -s|--status        Filter on status. Possible values: Actions Add An error occurred deleting item #%d. An error occurred while archiving this item. An error occurred while deleting this item. An error occurred while listing the items. If the problem persists, please contact support. An error occurred while saving this item. An error occurred while updating this item. Any Archive Archived Are you sure you want to archive this item? Are you sure you want to delete this item? Are you sure you want to delete this item?  Type 'yes' to continue:  Back Creation date Cron to handle item expiration Date Date: %s Delete Delete item Description Description: %s Element %s Element %s: Modification Export items (as CSV) Hello, world! ID: %s Invalid element identifier. Invalid item ID Item #%s not found. Item #%s:
 List/search items Modify Name Name: '%s' Nb by page New No item found. No item.
 Not set Pattern Pending Refused Reset Restore items (from CSV) Save Search Search page Show item Status Status: %s The access to this application is available by the following URL: <a href="%1">%1</a> The element '%s' has been archived successfully. The element '%s' has been created. The element '%s' has been deleted successfully. The element '%s' has been updated successfully. There are errors preventing this item from being saved. Please correct them before attempting to add this item. There are errors preventing this item from being saved. Please correct them before attempting to save your changes. This app contains some demo pages: This is a simple app to show the different possibilities and basic functionality. This item is already archived. Unspecified. User cancel Validated View You cannot archive this item. You cannot delete this item. You cannot edit this item. You have not made any changes to element '%s'. You must provide a valid ID. You must provide item ID. [ID] [input file path] [item ID] [output file path] [patterns] Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Benjamin Renard <brenard@easter-eggs.com>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
 %3 élément Éléments %1 à %2 sur %3 %d élément(s) -j/--just-try    Mode just-try : Ne supprime pas réellement les éléments expirés -m/--max-age     Limit d'expiration des éléments (en secondes, optionnel) -o|--orderby       Critère de tri de la liste. Valeurs possibles : -r|--reverse       Ordre inverse -s|--status        Filtrer sur le statut. Valeurs possibles : Actions Ajouter Une erreur est survenue en supprimant l'élément #%d. Une erreur est survenue en archivant cet élément. Une erreur est survenue en supprimant cet élément. Une erreur est survenue en listant les éléments. Si le problème persiste, merci de prendre contact avec le support. Une erreur est survenue en enregistrant cet élément. Une erreur est survenue en mettant à jour cet élément. Peu importe Archiver Archivé Êtes-vous sûre de vouloir archiver cet élément ? Êtes-vous sûre de vouloir supprimer cet élément ? Êtes-vous sûre de vouloir supprimer cet élément ? Taper 'yes' pour continuer :  Retour Date de création Cron gérant l'expiration des éléments Date Date : %s Supprimer Supprimer un élément Description Description : %s Élément %s Élément %s : Modification Exporter les éléments (au format CSV) Bonjour tout le monde ! ID : %s Identifiant d'élément invalid. ID d'élément invalid Élément #%s introuvable. Élément #%s :
 Lister/rechercher les éléments Modifier Nom Nom : %s Nb par page Nouveau Aucun élément trouvé. Aucun élément.
 Non-défini Not clé En attente Refusé Réinitialiser Restaurer les éléments (depuis un fichier CSV) Enregistrer Rechercher Page de recherche Voir un élément Statut Statut : %s L'accès à cette application est possible via l'URL suivante : <a href="%1">%1</a> L'élément '%s' a bien été archivé. L'élément '%s' a bien été créé. L'élément '%s' a bien été supprimé. L'élément '%s' a bien été mise à jour. Des erreurs empêchent l'enregistrement de cet élément. Merci de les corriger avant de tenter d'ajouter cet élément. Des erreurs empêchent l'enregistrement de cet élément. Merci de les corriger avant de tenter d'enregistrer vos modifications. Cette application contient quelques pages de démo : Ceci est une simple application pour montrer les différentes possibilités et les fonctionnalités de base. Cet élément est déjà archivé. Non-spécifié. L'utilisateur a annulé Validé Voir Vous ne pouvez pas archiver cet élément. Vous ne pouvez pas supprimer cet élément. Vous ne pouvez pas modifier cet élément. Vous n'avez apporté aucune modification à l'élément '%s'. Vous devez fournir un ID valid. Vous devez fournir un ID valid. [ID] [chemin du fichier d'entrée] [ID de l'élément] [chemin du fichier de sortie] [mots clés] 