{extends file='Tpl:empty.tpl'}
{block name="pagetitle"}{/block}
{block name="content"}
<div class="p-5 mb-4 bg-light rounded-3">
  <div class="container-fluid py-5">
    <h1 class="display-5 fw-bold">{t}Hello, world!{/t}</h1>
    <p class="lead">
      {t}This is a simple app to show the different possibilities and basic functionality.{/t}
    </p>
    <p class="lead">
      {t 1=App::get("public_root_url") escape=false}The access to this application is available by the following URL: <a href="%1">%1</a>{/t}
    </p>
    <hr class="my-4">
    <p>{t}This app contains some demo pages:{/t}</p>
    <a class="btn btn-primary btn-lg" href="item" role="button">{t}Search page{/t}</a>
  </div>
</div>
{/block}
