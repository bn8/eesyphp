{extends file='TplCore:empty.tpl'}
{block name="navbar-extra-content"}
{if isset($auth_user) && $auth_user}
<form class="d-flex" role="search" action='item'>
  <div class="input-group input-group-sm me-2">
    <input class="form-control" type="search" name="pattern"
      placeholder="{t}Search{/t}" aria-label="{t}Search{/t}">
    <span class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></span>
  </div>
</form>
{/if}
{/block}
