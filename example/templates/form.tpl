{extends file='Tpl:empty.tpl'}
{block name="content"}
<form enctype="multipart/form-data" method="post" class="container" id="item-form">
  <input type='hidden' name='session_key' value='{$session_key|escape:'quotes'}' />

  <div class="mb-3 row">
    <label class="col-sm-2 col-form-label required">{t}Name{/t}</label>
    <div class="col-sm-10">
      <input name="name" type="text" required
        value='{if $info->name}{$info->name|escape:'quotes'}{/if}'
        class="form-control{if array_key_exists('name', $field_errors)} is-invalid{else if $submitted} is-valid{/if}"/>
      {if array_key_exists('name', $field_errors)}
        <div class="invalid-feedback">{$field_errors['name']}</div>
      {/if}
    </div>
  </div>

  <div class="mb-3 row">
    <label class="col-sm-2 col-form-label required">{t}Status{/t}</label>
    <div class="col-sm-10">
      <select name="status" id="status" required
        class="form-select{if array_key_exists('status', $field_errors)} is-invalid{else if $submitted} is-valid{/if}">
        {html_options options=$statuses selected=$info->status}
      </select>
      {if array_key_exists('status', $field_errors)}
        <div class="invalid-feedback">{$field_errors['status']}</div>
      {/if}
    </div>
  </div>

  <div class="mb-3 row">
    <label class="col-sm-2 col-form-label">{t}Description{/t}</label>
    <div class="col-sm-10">
      <textarea name="description" id="description"
        class="form-control{if array_key_exists('description', $field_errors)} is-invalid{else if $submitted} is-valid{/if}"
        >{if $info->description}{$info->description|escape:"htmlall"}{/if}</textarea>
      {if array_key_exists('description', $field_errors)}
        <div class="form-error">{$field_errors['description']}</div>
      {/if}
    </div>
  </div>

  <div class="center">
    <a href="{if isset($item_id)}item/{$item_id}{else}search{/if}" class="btn btn-light">
      <i class="fa fa-undo"></i> {t}Back{/t}
    </a>
    <button type="submit" name="submit" value="submitted" class="btn btn-primary">
      {if isset($item_id)}
        <i class="fa fa-floppy-o" aria-hidden="true"></i> {t}Save{/t}
      {else}
        <i class="fa fa-plus" aria-hidden="true"></i> {t}Add{/t}
      {/if}
    </button>
  </div>
</form>
{/block}
