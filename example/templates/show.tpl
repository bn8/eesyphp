{extends file='Tpl:empty.tpl'}
{block name="content"}
<div class="container">
  <div class="mb-2 row">
    <label class="col-sm-4 col-form-label">{t}Name{/t}</label>
    <div class="col-sm-8">
      <span class="form-control-plaintext">
        {$item->name}
      </span>
    </div>
  </div>

  <div class="mb-2 row">
    <label class="col-sm-4 col-form-label">{t}Creation date{/t}</label>
    <div class="col-sm-8">
      <span class="form-control-plaintext">
        {format_time time=$item->date}
      </span>
    </div>
  </div>

  <div class="mb-2 row">
    <label class="col-sm-4 col-form-label">{t}Status{/t}</label>
    <div class="col-sm-8">
      <span class="form-control-plaintext">
        {item_status item=$item}
      </span>
    </div>
  </div>

  <div class="mb-2 row">
    <label class="col-sm-4 col-form-label">{t}Description{/t}</label>
    <div class="col-sm-8">
      <span class="form-control-plaintext">
        {if $item->description}{$item->description|escape:'htmlall'}{else}{t}Unspecified.{/t}{/if}
      </span>
    </div>
  </div>
</div>

<div class="center">
  <a href="item" class="btn btn-light"><i class="fa fa-undo"></i> {t}Back{/t}</a>
  {if can_modify($item)}<a href="item/{$item->id}/modify" class="btn btn-primary"><i class="fa fa-edit"></i> {t}Modify{/t}</a>{/if}
  {if can_archive($item)}<a data-myconfirm-url="item/{$item->id}/archive" data-myconfirm-question="{t}Are you sure you want to archive this item?{/t}" class="btn btn-warning myconfirm-link"><i class="fa fa-archive"></i> {t}Archive{/t}</a>{/if}
  {if can_delete($item)}<a href="#" data-myconfirm-url="item/{$item->id}/delete" data-myconfirm-question="{t}Are you sure you want to delete this item?{/t}" class="btn btn-danger myconfirm-link"><i class="fa fa-trash"></i> {t}Delete{/t}</a>{/if}
</div>
{/block}
