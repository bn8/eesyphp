{extends file='Tpl:empty.tpl'}
{block name="content"}
<form method='post'>
  <input type='hidden' name='session_key' value='{$session_key|escape:'quotes'}' />
  <div class="row">

    <div class="col">
      <input type="text" name="pattern" placeholder="{t}Pattern{/t}"
        class="form-control{if isset($pattern_error) && $pattern_error} is-invalid{/if}"
        value='{$search.pattern|escape:'quotes'}'/>
    </div>

    <div class="col">
      <div class="input-group mb-3">
        <label class="input-group-text" for="status">{t}Status{/t}</label>
        <select name="status" id="status"
          class="form-select{if isset($status_error) && $status_error} is-invalid{/if}">
          {html_options options=$status_choices selected=$search.filters.status}
        </select>
      </div>
    </div>

    <div class="col-md-2">
      <div class="input-group mb-3">
        <label class="input-group-text" for="nb_by_page">{t}Nb by page{/t}</label>
        <select name="nb_by_page" class="form-select">
          {html_options values=$nbs_by_page output=$nbs_by_page selected=$search.nb_by_page}
        </select>
      </div>
    </div>

    <div class="col-md-3">
      <div class="container">
        <div class="btn-group" role="group" aria-label="Actions">
          <button type="submit" name="submit" class="btn btn-primary">
            <i class="fa fa-search"></i> {t}Search{/t}
          </button>
          <a href="{$request->current_url}?clear=true" class="btn btn-warning">
            <i class="fa fa-trash"></i> {t}Reset{/t}
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-2 text-end">
      <a href="item/new" class="btn btn-success"><i class="fa fa-plus"></i> {t}Add{/t}</a>
    </div>
  </div>
</form>

<table class="table" style='margin-top: 1em;'>
  <thead>
    <tr>
      <th scope="col">
        {table_ordered_th url="{$request->current_url}" order="date" text="{t}Date{/t}" search=$search}
      </th>
      <th scope="col">
        {table_ordered_th url="{$request->current_url}" order="name" text="{t}Name{/t}" search=$search}
      </th>
      <th scope="col">{t}Status{/t}</th>
      <th scope="col" class="center">{t}Actions{/t}</th>
    </tr>
  </thead>
  <tbody>
  {foreach $result.items as $item}
    <tr>
      <td>{format_time time=$item->date}</td>
      <td>
        <a href="item/{$item->id}">
          <span title='{$item->id|escape:'quotes'}'>{$item->name}</span>
        </a>
      </td>
      <td>{item_status item=$item}</td>
      <td class="center">
        <a href="item/{$item->id}" class="btn btn-sm btn-info">
          <i class="fa fa-eye"></i> {t}View{/t}
        </a>
        {if can_modify($item)}
          <a href="item/{$item->id}/modify" class="btn btn-sm btn-primary">
            <i class="fa fa-edit"></i> {t}Modify{/t}
          </a>
        {/if}
        {if can_archive($item)}
          <a data-myconfirm-url="item/{$item->id}/archive"
            data-myconfirm-question="{t}Are you sure you want to archive this item?{/t}"
            class="btn btn-sm btn-warning myconfirm-link">
            <i class="fa fa-archive"></i> {t}Archive{/t}
          </a>
        {/if}
        {if can_delete($item)}
          <a href="#" data-myconfirm-url="item/{$item->id}/delete"
            data-myconfirm-question="{t}Are you sure you want to delete this item?{/t}"
            class="btn btn-sm btn-danger myconfirm-link">
            <i class="fa fa-trash"></i> {t}Delete{/t}
          </a>
        {/if}
      </td>

    </tr>
  {foreachelse}
    <tr>
      <td colspan="4" class="center">{t}No item found.{/t}</td>
    </tr>
  {/foreach}
  </tbody>
</table>

<div class="row">
  {if $result.count>1}
  <div class="col-lg-3">
    <div role="status">{t count=$result.count 1=$result.first 2=$result.last 3=$result.count plural="Elements %1 to %2 on %3"}%3 element{/t}</div>
  </div>
  {/if}
  {if $result.nb_pages > 1}
  <div class="col-lg-9">
    <nav aria-label="Navigation par page">
      <ul class="pagination justify-content-end">
        <li{if $result.page==1} class="disabled"{/if}>
          <a href="{$request->current_url}?page=1" aria-label="Première page" class="page-link"><span aria-hidden="true">&laquo;&laquo;</span></a>
        </li>
        <li{if $result.page==1} class="disabled"{/if}>
          <a href="{$request->current_url}?page={$result.page-1}" aria-label="Page précédente" class="page-link"><span aria-hidden="true">&laquo;</span></a>
        </li>
        {if $result.nb_pages > 9}
          {if $result.page > 4}
            {if $result.page > $result.nb_pages-4}
              {assign var=start value=$result.nb_pages-9}
             {else}
              {assign var=start value=$result.page-5}
            {/if}
          {else}
            {assign var=start value=0}
          {/if}
          {foreach from=1|range:9 item=i}
            <li class="paginate_button{if $result.page==$start+$i} active{/if}"><a href="{$request->current_url}?page={$start+$i}" class="page-link">{$start+$i}</a></li>
          {/foreach}
        {else}
          {section name=listpage start=1 loop=$result.nb_pages+1 step=1}
            <li class="paginate_button{if $result.page == $smarty.section.listpage.index} active{/if}"><a href="{$request->current_url}?page={$smarty.section.listpage.index}" class="page-link">{$smarty.section.listpage.index}</a></li>
          {/section}
        {/if}
        <li{if $result.page==$result.nb_pages} class="disabled"{/if}>
          <a href="{$request->current_url}?page={$result.page+1}" aria-label="Page suivante" class="page-link"><span aria-hidden="true">&raquo;</span></a>
        </li>
        <li{if $result.page==$result.nb_pages} class="disabled"{/if}>
          <a href="{$request->current_url}?page={$result.nb_pages}" aria-label="Dernière page" class="page-link"><span aria-hidden="true">&raquo;&raquo;</span></a>
        </li>
      </ul>
    </nav>
  </div>
  {/if}
</div>
{/block}
