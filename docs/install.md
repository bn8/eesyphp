# Installation

## Dependencies :

Some Debian packages have to be installed :

```bash
apt install git composer php-cli php-yaml php-json php-mbstring php-intl
# for mailing
apt install php-mail php-mail-mine php-net-smtp php-auth-sasl
# for PostgreSQL DB backend
apt install php-pgsql
# for MySQL/MariaDB DB backend
apt install php-mysql
# for sqlite DB backend
apt install sqlite3 php-sqlite3
# for CAS authentication
apt install php-xml
# for LDAP authentication
apt install php-ldap php-net-ldap2
```

## Deployment :

To deploy this application, for instance in _/var/www/eesyphp_, run this commands :

```bash
cd /var/www
adduser --home /var/www/eesyphp --no-create-home --disabled-password --gid 33 eesyphp
git clone https://gogs.example.org/example/eesyphp.git /var/www/eesyphp
chown eesyphp: /var/www/eesyphp
ln -s /var/www/eesyphp/bin/items.wrapper /usr/local/bin/eesyphp
ln -s /var/www/eesyphp/docs/eesyphp.cron /etc/cron.d/eesyphp
ln -s /var/www/eesyphp/docs/eesyphplogrotate /etc/logrotate.d/eesyphp
```

## Install PHP dependencies :

```bash
su - eesyphp
composer install
```

## Initialize database :

### SQLite

To initialize SQLite database, run this commands :

```bash
cd /var/www/eesyphp/data
sqlite3 db.sqlite3 < sqlite.init-db.sql
```

### PostgreSQL

To initialize PostgreSQL database, run this commands :

```bash
su - postgres
createuser -P items
createdb -O items items
psql items < /var/www/eesyphp/data/pgsql.init-db.sql
```

### MariaDB / MySQL

To initialize MariaDB / MySQL database, run this commands :

```bash
sudo mysql << EOF
CREATE DATABASE items;
GRANT ALL ON items.* TO items@localhost IDENTIFIED BY 'items';
EOF
sudo mysql items < /var/www/eesyphp/data/mariadb-mysql.init-db.sql
```

## Configure PHP FPM pool :

You could configure a dedicated _PHP FPM pool_.

**_Example :_**

```
[eesyphp]
user = eesyphp
group = www-data
listen = /run/php/php7.3-fpm-eesyphp.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
chdir = /
php_value[error_log] = /var/www/eesyphp/data/logs/php.log
php_value[upload_max_filesize] = 10M
php_value[post_max_size] = 10M
php_value[memory_limit] = 10M
php_value[default_charset] = 'UTF-8'
php_value[date.timezone] = Europe/Paris
```

## Configure Apache VirtualHost :

You could configure a dedicated _VirtualHost_.

**_Example :_**

```
<VirtualHost *:80>
        ServerName eesyphp.example.org
        DocumentRoot /var/www/empty

        RewriteEngine on
        RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]

        ErrorLog  /var/log/apache2/eesyphp.example.org-error.log
        CustomLog /var/log/apache2/eesyphp.example.org-access.log combined
</VirtualHost>

<VirtualHost *:443>
        ServerName eesyphp.example.org
        DocumentRoot /var/www/eesyphp/public_html

        SSLEngine On
        SSLCertificateFile /etc/letsencrypt/live/eesyphp.example.org/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/eesyphp.example.org/privkey.pem

        <FilesMatch \.php$>
                SetHandler "proxy:unix:/run/php/php7.3-fpm-eesyphp.sock|fcgi:///"
        </FilesMatch>

        <Directory   /var/www/eesyphp/public_html>
                # Acceptable overrides:
                # - FileInfo (.htacces-based rewrite rules)
                # - AuthConfig (.htaccess-based basic auth)
                AllowOverride FileInfo AuthConfig
        </Directory>

        # Compress most static files
        AddOutputFilterByType DEFLATE text/html text/xml text/css application/javascript

        ErrorLog  /var/log/apache2/eesyphp.example.org-error.log
        CustomLog /var/log/apache2/eesyphp.example.org-access.log combined
</VirtualHost>
```
