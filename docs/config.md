# Configuration

The main configuration file is _includes/config.inc.php_. You can overrides all variables that main configuration file contain in your own configuration file _includes/config.local.php_. This file will be automatically included if present.

> **Note :** In the configuration files, to refer to the application root directory, you could use the variable `$root_dir_path` (that does not contain the trailing slash).

**The first thing you have to configured is the public root URL of the API in `$public_root_url` variable.** This URL could be relative (default) or absolute.

> **Note :** You also could adapt _$main_pagetitle_ and _$included_css_files_ to your situation.

## Data/files directories :

All application data are, by default, in _data_ sub-directory. You could configure it by using the following variables :

- `$data_dir` : Root DATA directory (Default : _data_)
- `$tmp_root_dir` : Root temporary files directory (Default : _$data_dir/tmp_)
- `$logs_dir_path` : Logs directory (Default : _$data_dir/logs_)
- `$upload_tmp_dir` : Temporary directory for PHP uploading files : when uploading files, PHP will stored files in this directory before application move it to their final destination (Default : _$data_dir/uploading_)
- `$smarty_templates_c_dir` : Templates cache directory (Default : _$tmp_root_dir/templates_c_)

## Logs :

Default log files path is _data/logs/app.log_ (and _data/logs/cli.log_ for CLI tool). This could be configured using `$log_file` configuration variable. You can also set the log level using `$log_level` variable _(default value = "INFO", see config.inc.php)._

## Database configuration :

The database is used to store demo items information. For this reason, a basic _Sqlite_ database could be used and is the default database backend. However, you can use any database backend supported by the [PHP PDO abstraction system](http://php.net/pdo).

The database connection is configured using the following variables :

- `$dn_dsn` : the _Data Source Name (DSN)_ depending on your database backend.
- `$db_user` : the database username.
- `$db_pwd` : the database password.
- `$db_options` : the database connection options (see your PHP PDO Driver documentation). By default, this variable value is an empty array ("`array()`").
- `$db_date_format` : the date format as retrieve from the database.
- `$db_datetime_format` : the date and time format as retrieve from the database.

> **Note :** You could find some configuration examples for _PostgreSQL_, _MariaDB / MySQL_ or _Sqlite_ in _config.inc.php_ file. Initial _SQL_ files could also be found in _data_ directory.

## E-mail configuration :

This application send some mail by using [PEAR Mail library](http://pear.php.net/package/Mail). The path of this library must be specify using `$php_mail_path`. The default value corresponding to the version provided by _php-mail_ Debian package.

This library support different methods to send mail and the default one is _SMTP_. This method require that the [PEAR Net_SMTP library](http://pear.php.net/package/Net_SMTP) is also installed on the system. You could configure another method sending method using the variable `$mail_send_method` (see comment in _config.inc.php_ file or [the PEAR Mail library documentation](http://pear.php.net/manual/en/package.mail.mail.factory.php)).

Depending on the sending method choose, some options are available to configure their usage (see comment in _config.inc.php_ file or [the PEAR Mail library documentation](http://pear.php.net/manual/en/package.mail.mail.factory.php)) and could be configured using `$mail_send_params` variable.

To forge e-mail, this application use [PEAR Mail_Mime library](http://pear.php.net/package/Mail_Mime). The path of this library must be specify using `$php_mail_mime_path`. The default value corresponding to the version provided by _php-mail-mime_ Debian package.

Some other variables exist to manage the email sending by the application :

- `$mail_headers` : an PHP array of default headers add to mail sent by the application
- `$mail_sender` : the email address of the sender of all emails sent by the application
- `$mail_catch_all` : for debugging purpose, you can using this variable to specify an email address that will received all email sent by the application in place of the original recipient

## Web stats JS code inclusion

If you use a Web stats tool that need to include a piece of JS code in all page like [Matomo](https://matomo.org/), you could defined the `$webstats_js_code` variable.

## Sentry integration

If you want to enable the [Sentry](https://sentry.io) integration, you have to define the `$sentry_dsn`. This integration permit to report PHP errors (see `$sentry_php_error_types`) and exception as issues and to monitor performance of the application (see `$sentry_traces_sample_rate`).
