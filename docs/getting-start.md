# Getting start

To start using the EesyPHP framework on your project, you have to firstly install it using [composer](https://getcomposer.org/) :

```bash
mkdir my_project
cd my_project
composer init --stability dev
# When asking for dependency, search brenard/eesyphp
composer update
```

With the provided `eesyphp` CLI tool, you can quickly init a new project:

```bash
vendor/bin/eesyphp new_project
```
